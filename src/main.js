import Vue from 'vue'

/* ============
 * Styles
 * ============
 */
import '@/styles/main.scss'

/* ============
 * Directives
 * ============
 */
import '@/directives/style'

/* ============
 * Mixins
 * ============
 */
import TimeMixin from '@/mixins/time'
import TextMixin from '@/mixins/text'

/* ============
 * Plugins
 * ============
 */

import '@/plugins/vuex'
import '@/plugins/vuex-router-sync'
import '@/plugins/moment'
import router from '@/plugins/vue-router'
import i18n from '@/plugins/vue-i18n'
import '@/plugins/vee-validate'
import vuetify from '@/plugins/vuetify'

/* ============
 * Main App
 * ============
 *
 * Last but not least, we import the main application.
 */
import App from '@/App'
import store from '@/store'

Vue.config.productionTip = false

Vue.mixin(TimeMixin)
Vue.mixin(TextMixin)

new Vue({
  store,
  router,
  i18n,
  vuetify,
  render: h => h(App)
}).$mount('#app')
