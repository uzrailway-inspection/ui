/* ============
 * Mutations for the district state
 * ============
 *
 * The mutations that are available on the
 * district state.
 */

import {
  SET_DISTRICT_LIST,
  SET_DISTRICT_DETAIL
} from './mutation-types'

/* eslint-disable no-param-reassign */
export default {
  [SET_DISTRICT_LIST] (state, list) {
    state.list = list
  },
  [SET_DISTRICT_DETAIL] (state, detail) {
    state.detail = detail
  }
}
