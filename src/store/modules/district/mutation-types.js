/* ============
 * Mutation types for the district state
 * ============
 *
 */

export const SET_DISTRICT_LIST = 'SET_DISTRICT_LIST'
export const SET_DISTRICT_DETAIL = 'SET_DISTRICT_DETAIL'

export default {
  SET_DISTRICT_LIST,
  SET_DISTRICT_DETAIL
}
