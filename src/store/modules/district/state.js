/* ============
 * State of the district module
 * ============
 *
 * The initial state of the district module
 */

export default {
  list: {},
  detail: null
}
