/* ============
 * Mutation types for the category state
 * ============
 *
 */

export const SET_CATEGORY_LIST = 'SET_CATEGORY_LIST'
export const SET_CATEGORY_DETAIL = 'SET_CATEGORY_DETAIL'

export default {
  SET_CATEGORY_LIST,
  SET_CATEGORY_DETAIL
}
