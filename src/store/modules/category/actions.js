/* ============
 * Actions for the category state
 * ============
 *
 */

import path from 'ramda/src/path'
import { sprintf } from 'sprintf-js'
import router from '@/plugins/vue-router'
import axios from '@/plugins/axios'
import i18n from '@/plugins/vue-i18n'
import toSnakeCase from '@/helpers/toSnakeCase'
import * as ACTION from '@/constants/actionTypes'
import * as API from '@/constants/api'
import { SET_CATEGORY_LIST, SET_CATEGORY_DETAIL } from './mutation-types'
import { getIdsFromUrl } from '@/helpers/objects'

export default {
  [ACTION.GET_CATEGORY_LIST_ACTION] ({ commit }) {
    const params = path(['history', 'current', 'query'], router)
    commit(SET_CATEGORY_LIST, [])
    return axios.get(API.GET_CATEGORY_LIST, { params })
      .then(response => {
        commit(SET_CATEGORY_LIST, response.data)
      })
  },
  [ACTION.CATEGORY_CREATE_ACTION] ({ commit }, form) {
    return axios.post(API.CATEGORY_CREATE, toSnakeCase(form))
  },
  [ACTION.CATEGORY_DETAIL_ACTION] ({ commit }, id) {
    const url = sprintf(API.CATEGORY_DETAIL, id)
    commit(SET_CATEGORY_DETAIL, {})
    return axios.get(url).then(response => {
      commit(SET_CATEGORY_DETAIL, response.data)
      return response
    })
  },
  [ACTION.CATEGORY_UPDATE_ACTION] ({ commit }, id) {
    const url = sprintf(API.CATEGORY_DETAIL, id)
    return (form) => axios.patch(url, toSnakeCase(form)).then(() => {
      commit(SET_CATEGORY_DETAIL, {})
    })
  },
  [ACTION.CATEGORY_DELETE_ACTION] ({ commit, dispatch }, id) {
    const url = sprintf(API.CATEGORY_DETAIL, id)
    commit(SET_CATEGORY_LIST, [])
    return axios.delete(url).then(() => {
      dispatch(ACTION.NOTIFY_ACTION, {
        content: i18n.t('Successfully deleted'),
        value: true,
        color: 'success'
      }, { root: true })
    }).catch(() => {
      dispatch(ACTION.NOTIFY_ACTION, {
        content: i18n.t('Error on deleting'),
        value: true,
        color: 'error'
      }, { root: true })
    }).finally(() => {
      dispatch(ACTION.GET_CATEGORY_LIST_ACTION)
    })
  },
  [ACTION.CATEGORY_BULK_DELETE_ACTION] ({ commit, dispatch }) {
    const ids = getIdsFromUrl(path(['history', 'current', 'query'], router))
    commit(SET_CATEGORY_LIST, [])
    return axios.delete(API.CATEGORY_BULK_DELETE, { data: { ids } }).then(() => {
      dispatch(ACTION.NOTIFY_ACTION, {
        content: i18n.t('Successfully deleted'),
        value: true,
        color: 'success'
      }, { root: true })
    }).catch(() => {
      dispatch(ACTION.NOTIFY_ACTION, {
        content: i18n.t('Error on deleting'),
        value: true,
        color: 'error'
      }, { root: true })
    }).finally(() => {
      dispatch(ACTION.GET_CATEGORY_LIST_ACTION)
    })
  }
}
