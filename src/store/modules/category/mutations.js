/* ============
 * Mutations for the snackbar state
 * ============
 *
 * The mutations that are available on the
 * snackbar state.
 */

import {
  SET_CATEGORY_LIST,
  SET_CATEGORY_DETAIL
} from './mutation-types'

/* eslint-disable no-param-reassign */
export default {
  [SET_CATEGORY_LIST] (state, list) {
    state.list = list
  },
  [SET_CATEGORY_DETAIL] (state, detail) {
    state.detail = detail
  }
}
