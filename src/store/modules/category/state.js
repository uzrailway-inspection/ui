/* ============
 * State of the category module
 * ============
 *
 * The initial state of the category module
 */

export default {
  list: {},
  detail: {}
}
