/* ============
 * Mutations for the revision state
 * ============
 *
 * The mutations that are available on the
 * revision state.
 */

import {
  SET_REVISION_LIST
} from './mutation-types'

/* eslint-disable no-param-reassign */
export default {
  [SET_REVISION_LIST] (state, list) {
    state.list = list
  }
}
