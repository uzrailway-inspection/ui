/* ============
 * Actions for the revision state
 * ============
 *
 */

import path from 'ramda/src/path'
import prop from 'ramda/src/prop'
import axios from '@/plugins/axios'
import router from '@/plugins/vue-router'
import toSnakeCase from '@/helpers/toSnakeCase'
import * as ACTION from '@/constants/actionTypes'
import * as API from '@/constants/api'
import { SET_REVISION_LIST } from './mutation-types'
import { sprintf } from 'sprintf-js'

export default {
  [ACTION.GET_REVISION_LIST_ACTION] ({ commit }) {
    const params = path(['history', 'current', 'query'], router)
    commit(SET_REVISION_LIST, [])
    return axios.get(API.GET_REVISION_LIST, { params })
      .then(response => {
        commit(SET_REVISION_LIST, response.data)
      })
  },
  [ACTION.REVISION_CREATE_ACTION] ({ commit }, form) {
    const data = toSnakeCase({
      ...form,
      company: prop('id', form.company)
    })
    return axios.post(API.REVISION_CREATE, data)
  },
  [ACTION.REVISION_CONFIRM_ACTION] ({ commit }, id) {
    const url = sprintf(API.REVISION_CONFIRM, id)
    return axios.post(url)
  }
}
