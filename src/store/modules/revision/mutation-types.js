/* ============
 * Mutation types for the revision state
 * ============
 *
 */

export const SET_REVISION_LIST = 'SET_REVISION_LIST'

export default {
  SET_REVISION_LIST
}
