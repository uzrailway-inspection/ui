/* ============
 * Mutations for the user group state
 * ============
 *
 * The mutations that are available on the
 * user group state.
 */

import {
  SET_GROUP_LIST,
  SET_GROUP_DETAIL
} from './mutation-types'

/* eslint-disable no-param-reassign */
export default {
  [SET_GROUP_LIST] (state, list) {
    state.list = list
  },
  [SET_GROUP_DETAIL] (state, detail) {
    state.detail = detail
  }
}
