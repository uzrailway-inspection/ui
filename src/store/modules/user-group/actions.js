/* ============
 * Actions for the user group state
 * ============
 *
 */

import path from 'ramda/src/path'
import prop from 'ramda/src/prop'
import map from 'ramda/src/map'
import { sprintf } from 'sprintf-js'
import axios from '@/plugins/axios'
import router from '@/plugins/vue-router'
import i18n from '@/plugins/vue-i18n'
import toSnakeCase from '@/helpers/toSnakeCase'
import * as ACTION from '@/constants/actionTypes'
import * as API from '@/constants/api'
import { SET_GROUP_DETAIL, SET_GROUP_LIST } from './mutation-types'

export default {
  [ACTION.GET_GROUP_LIST_ACTION] ({ commit }) {
    const params = path(['history', 'current', 'query'], router)
    commit(SET_GROUP_LIST, [])
    return axios.get(API.GET_GROUP_LIST, { params })
      .then(response => {
        commit(SET_GROUP_LIST, response.data)
      })
  },
  [ACTION.GROUP_CREATE_ACTION] ({ commit }, form) {
    const data = toSnakeCase({
      ...form,
      permissions: map(prop('id'), form.permissions)
    })
    return axios.post(API.GROUP_CREATE, toSnakeCase(data))
  },
  [ACTION.GROUP_DETAIL_ACTION] ({ commit }, id) {
    const url = sprintf(API.GROUP_DETAIL, id)
    commit(SET_GROUP_DETAIL, {})
    return axios.get(url).then(response => {
      commit(SET_GROUP_DETAIL, response.data)
      return response
    })
  },
  [ACTION.GROUP_UPDATE_ACTION] ({ commit, rootState, dispatch }, id) {
    const url = sprintf(API.GROUP_DETAIL, id)
    return (form) => {
      const data = toSnakeCase({
        ...form,
        permissions: map(prop('id'), form.permissions)
      })
      return axios.patch(url, toSnakeCase(data))
    }
  },
  [ACTION.GROUP_DELETE_ACTION] ({ commit, dispatch }, id) {
    const url = sprintf(API.GROUP_DETAIL, id)
    commit(SET_GROUP_LIST, [])
    return axios.delete(url).then(() => {
      dispatch(ACTION.NOTIFY_ACTION, {
        content: i18n.t('Successfully deleted'),
        value: true,
        color: 'success'
      }, { root: true })
    }).catch(() => {
      dispatch(ACTION.NOTIFY_ACTION, {
        content: i18n.t('Error on deleting'),
        value: true,
        color: 'error'
      }, { root: true })
    }).finally(() => {
      dispatch(ACTION.GET_GROUP_LIST_ACTION)
    })
  }
}
