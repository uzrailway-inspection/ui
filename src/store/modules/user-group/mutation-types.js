/* ============
 * Mutation types for the user group state
 * ============
 *
 */

export const SET_GROUP_LIST = 'SET_GROUP_LIST'
export const SET_GROUP_DETAIL = 'SET_GROUP_DETAIL'

export default {
  SET_GROUP_LIST,
  SET_GROUP_DETAIL
}
