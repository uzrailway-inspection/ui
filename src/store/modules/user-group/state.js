/* ============
 * State of the user group module
 * ============
 *
 * The initial state of the user group module
 */

export default {
  list: {},
  detail: null
}
