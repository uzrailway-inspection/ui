/* ============
 * Actions for the region state
 * ============
 *
 */

import path from 'ramda/src/path'
import { sprintf } from 'sprintf-js'
import axios from '@/plugins/axios'
import router from '@/plugins/vue-router'
import i18n from '@/plugins/vue-i18n'
import toSnakeCase from '@/helpers/toSnakeCase'
import * as ACTION from '@/constants/actionTypes'
import * as API from '@/constants/api'
import { SET_REGION_LIST, SET_REGION_DETAIL } from './mutation-types'
import { getIdsFromUrl } from '@/helpers/objects'

export default {
  [ACTION.GET_REGION_LIST_ACTION] ({ commit }) {
    const params = path(['history', 'current', 'query'], router)
    commit(SET_REGION_LIST, [])
    return axios.get(API.GET_REGION_LIST, { params })
      .then(response => {
        commit(SET_REGION_LIST, response.data)
      })
  },
  [ACTION.REGION_CREATE_ACTION] ({ commit }, form) {
    return axios.post(API.REGION_CREATE, toSnakeCase(form))
  },
  [ACTION.REGION_DETAIL_ACTION] ({ commit }, id) {
    const url = sprintf(API.REGION_DETAIL, id)
    commit(SET_REGION_DETAIL, {})
    return axios.get(url).then(response => {
      commit(SET_REGION_DETAIL, response.data)
      return response
    })
  },
  [ACTION.REGION_UPDATE_ACTION] ({ commit }, id) {
    const url = sprintf(API.REGION_DETAIL, id)
    return (form) => axios.patch(url, toSnakeCase(form)).then(() => {
      commit(SET_REGION_DETAIL, {})
    })
  },
  [ACTION.REGION_DELETE_ACTION] ({ commit, dispatch }, id) {
    const url = sprintf(API.REGION_DETAIL, id)
    commit(SET_REGION_LIST, [])
    return axios.delete(url).then(() => {
      dispatch(ACTION.NOTIFY_ACTION, {
        content: i18n.t('Successfully deleted'),
        value: true,
        color: 'success'
      }, { root: true })
    }).catch(() => {
      dispatch(ACTION.NOTIFY_ACTION, {
        content: i18n.t('Error on deleting'),
        value: true,
        color: 'error'
      }, { root: true })
    }).finally(() => {
      dispatch(ACTION.GET_REGION_LIST_ACTION)
    })
  },
  [ACTION.REGION_BULK_DELETE_ACTION] ({ commit, dispatch }) {
    const ids = getIdsFromUrl(path(['history', 'current', 'query'], router))
    commit(SET_REGION_LIST, [])
    return axios.delete(API.REGION_BULK_DELETE, { data: { ids } }).then(() => {
      dispatch(ACTION.NOTIFY_ACTION, {
        content: i18n.t('Successfully deleted'),
        value: true,
        color: 'success'
      }, { root: true })
    }).catch(() => {
      dispatch(ACTION.NOTIFY_ACTION, {
        content: i18n.t('Error on deleting'),
        value: true,
        color: 'error'
      }, { root: true })
    }).finally(() => {
      dispatch(ACTION.GET_REGION_LIST_ACTION)
    })
  }
}
