/* ============
 * Mutation types for the region state
 * ============
 *
 */

export const SET_REGION_LIST = 'SET_REGION_LIST'
export const SET_REGION_DETAIL = 'SET_REGION_DETAIL'

export default {
  SET_REGION_LIST,
  SET_REGION_DETAIL
}
