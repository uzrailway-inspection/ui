/* ============
 * Mutations for the region state
 * ============
 *
 * The mutations that are available on the
 * region state.
 */

import {
  SET_REGION_LIST,
  SET_REGION_DETAIL
} from './mutation-types'

/* eslint-disable no-param-reassign */
export default {
  [SET_REGION_LIST] (state, list) {
    state.list = list
  },
  [SET_REGION_DETAIL] (state, detail) {
    state.detail = detail
  }
}
