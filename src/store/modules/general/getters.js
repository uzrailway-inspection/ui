/* ============
 * Getters for the general state
 * ============
 *
 */
import any from 'ramda/src/any'
import includes from 'ramda/src/includes'
import __ from 'ramda/src/__'
import is from 'ramda/src/is'

export const getToken = state => state.token
export const myPermissions = state => state.me.userPermissions
export const isSuperuser = state => state.me.isSuperuser
export const hasAccess = (state, { myPermissions, isSuperuser }) => permissions => {
  if (!permissions || isSuperuser) return true
  if (myPermissions) {
    if (is(Array, permissions)) {
      return any(includes(__, myPermissions), permissions)
    }
    if (is(String, permissions)) {
      return includes(permissions, myPermissions)
    }
  }
  return true
}
export const isDarkTheme = state => state.dark

export default {
  getToken,
  myPermissions,
  isSuperuser,
  hasAccess,
  isDarkTheme
}
