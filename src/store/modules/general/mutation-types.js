/* ============
 * Mutation types for the general state
 * ============
 *
 */

export const LOG_IN = 'LOG_IN'
export const LOG_OUT = 'LOG_OUT'
export const ME = 'ME'
export const NOTIFY = 'NOTIFY'
export const SET_PERMISSIONS = 'SET_PERMISSIONS'
export const SET_THEME = 'SET_THEME'

export default {
  LOG_IN,
  LOG_OUT,
  ME,
  NOTIFY,
  SET_PERMISSIONS,
  SET_THEME
}
