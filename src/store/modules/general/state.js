/* ============
 * State of the general state
 * ============
 *
 * The initial state of the general state
 */

import { getStorage } from '@/helpers/localStorage'
import { getSession } from '@/helpers/localSession'

export default {
  token: getSession('token') || getStorage('token'),
  me: null,
  permissions: [],
  dark: getStorage('dark'),
  snackbar: {
    content: '',
    color: 'success',
    value: undefined,
    left: false,
    top: true,
    bottom: false,
    multiLine: false,
    timeout: 6000,
    absolute: false,
    vertical: false
  }
}
