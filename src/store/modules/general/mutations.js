/* ============
 * Mutations for the general state
 * ============
 *
 * The mutations that are available on the
 * general state.
 */

import {
  LOG_IN,
  LOG_OUT,
  ME,
  NOTIFY,
  SET_PERMISSIONS,
  SET_THEME
} from './mutation-types'
import { removeStorage, setStorage } from '@/helpers/localStorage'
import { removeSession, setSession } from '@/helpers/localSession'
import router from '@/plugins/vue-router'
import { N_LOGIN } from '@/constants/routes'

export default {
  [LOG_IN] (state, { token, remember }) {
    state.token = token
    if (remember) {
      return setStorage('token', token)
    }
    return setSession('token', token)
  },
  [LOG_OUT] (state) {
    state.token = null
    state.me = null
    removeStorage('token')
    removeSession('token')
    router.push({
      name: N_LOGIN
    })
  },
  [ME] (state, data) {
    state.me = data
  },
  [SET_THEME] (state, dark) {
    state.dark = dark
    setStorage('dark', dark)
  },
  [SET_PERMISSIONS] (state, data) {
    state.permissions = data
  },
  [NOTIFY] (state, options) {
    for (const option in options) {
      if (Object.prototype.hasOwnProperty.call(options, option)) {
        state.snackbar[option] = options[option]
      }
    }
  }
}
