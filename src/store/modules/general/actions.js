/* ============
 * Actions for the general state
 * ============
 *
 */

import path from 'ramda/src/path'
import join from 'ramda/src/join'
import pathOr from 'ramda/src/pathOr'
import compose from 'ramda/src/compose'
import prop from 'ramda/src/prop'
import { router } from '@/plugins/vue-router'
import axios from '@/plugins/axios'
import * as ACTION from '@/constants/actionTypes'
import * as API from '@/constants/api'
import { LOG_IN, LOG_OUT, ME, NOTIFY, SET_PERMISSIONS, SET_THEME } from './mutation-types'
import { RATING_LIST } from '@/constants/routes'

export default {
  [ACTION.NOTIFY_ACTION] ({ commit }, options) {
    commit(NOTIFY, options)
  },
  [ACTION.TOGGLE_THEME_ACTION] ({ commit, state }) {
    commit(SET_THEME, !state.dark)
  },
  [ACTION.LOGIN_ACTION] ({ commit, dispatch, state }, { remember, ...params }) {
    return axios.post(API.LOGIN, params)
      .then(response => {
        const token = path(['data', 'token'], response)
        commit(LOG_IN, {
          token,
          remember
        })
        router.push({
          name: RATING_LIST
        })
      })
      .catch(error => {
        const message = compose(
          join('<br/>'),
          pathOr([], ['response', 'data', 'nonFieldErrors'])
        )(error)
        dispatch(ACTION.NOTIFY_ACTION, {
          content: message,
          value: true,
          color: 'error'
        })
      })
  },
  [ACTION.LOGOUT_ACTION] ({ commit }) {
    return commit(LOG_OUT)
  },
  [ACTION.GET_PERMISSIONS_ACTION] ({ commit, dispatch }) {
    return axios.get(API.GET_PERMISSIONS)
      .then(response => {
        const permissions = prop(['data'], response)
        commit(SET_PERMISSIONS, permissions)
      })
  },
  [ACTION.GET_ME_ACTION] ({ commit, dispatch }) {
    return axios.get(API.ME)
      .then(response => {
        const me = prop(['data'], response)
        commit(ME, me)
      })
      .catch(error => {
        const message = compose(
          join('<br/>'),
          pathOr([], ['response', 'data', 'nonFieldErrors'])
        )(error)
        dispatch(ACTION.NOTIFY_ACTION, {
          content: message,
          value: true
        })
      })
  }
}
