/* ============
 * Mutations for the user state
 * ============
 *
 * The mutations that are available on the
 * user state.
 */

import {
  SET_USER_LIST,
  SET_USER_DETAIL
} from './mutation-types'

/* eslint-disable no-param-reassign */
export default {
  [SET_USER_LIST] (state, list) {
    state.list = list
  },
  [SET_USER_DETAIL] (state, detail) {
    state.detail = detail
  }
}
