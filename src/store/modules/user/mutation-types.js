/* ============
 * Mutation types for the user state
 * ============
 *
 */

export const SET_USER_LIST = 'SET_USER_LIST'
export const SET_USER_DETAIL = 'SET_USER_DETAIL'

export default {
  SET_USER_LIST,
  SET_USER_DETAIL
}
