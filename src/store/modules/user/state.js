/* ============
 * State of the user module
 * ============
 *
 * The initial state of the user module
 */

export default {
  list: {},
  detail: null
}
