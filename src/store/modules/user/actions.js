/* ============
 * Actions for the user state
 * ============
 *
 */

import path from 'ramda/src/path'
import prop from 'ramda/src/prop'
import map from 'ramda/src/map'
import { sprintf } from 'sprintf-js'
import axios from '@/plugins/axios'
import router from '@/plugins/vue-router'
import i18n from '@/plugins/vue-i18n'
import toSnakeCase from '@/helpers/toSnakeCase'
import * as ACTION from '@/constants/actionTypes'
import * as API from '@/constants/api'
import { SET_USER_LIST, SET_USER_DETAIL } from './mutation-types'
import { getIdsFromUrl } from '@/helpers/objects'

export default {
  [ACTION.GET_USER_LIST_ACTION] ({ commit }) {
    const params = path(['history', 'current', 'query'], router)
    commit(SET_USER_LIST, [])
    return axios.get(API.GET_USER_LIST, { params })
      .then(response => {
        commit(SET_USER_LIST, response.data)
      })
  },
  [ACTION.USER_CREATE_ACTION] ({ commit }, form) {
    const group = prop('id', form.groups)
    const groups = group ? [group] : undefined
    const data = toSnakeCase({
      ...form,
      avatar: prop('id', form.avatar),
      region: map(prop('id'), form.region),
      groups
    })
    return axios.post(API.USER_CREATE, toSnakeCase(data))
  },
  [ACTION.USER_DETAIL_ACTION] ({ commit }, id) {
    const url = sprintf(API.USER_DETAIL, id)
    commit(SET_USER_DETAIL, {})
    return axios.get(url).then(response => {
      if (response.data.groups.length) {
        response.data.groups = response.data.groups[0]
      } else {
        response.data.groups = null
      }
      commit(SET_USER_DETAIL, response.data)
      return response
    })
  },
  [ACTION.USER_UPDATE_ACTION] ({ commit, rootState, dispatch }, id) {
    const url = sprintf(API.USER_DETAIL, id)
    return (form) => {
      const group = prop('id', form.groups)
      const groups = group ? [group] : undefined
      const data = toSnakeCase({
        ...form,
        avatar: prop('id', form.avatar),
        region: map(prop('id'), form.region),
        groups
      })
      return axios.patch(url, toSnakeCase(data)).then(response => {
        if (response.data.id === rootState.me.id) {
          dispatch(ACTION.GET_ME_ACTION, null, { root: true })
        }
        return response
      })
    }
  },
  [ACTION.USER_DELETE_ACTION] ({ commit, dispatch }, id) {
    const url = sprintf(API.USER_DETAIL, id)
    commit(SET_USER_LIST, [])
    return axios.delete(url).then(() => {
      dispatch(ACTION.NOTIFY_ACTION, {
        content: i18n.t('Successfully deleted'),
        value: true,
        color: 'success'
      }, { root: true })
    }).catch(() => {
      dispatch(ACTION.NOTIFY_ACTION, {
        content: i18n.t('Error on deleting'),
        value: true,
        color: 'error'
      }, { root: true })
    }).finally(() => {
      dispatch(ACTION.GET_USER_LIST_ACTION)
    })
  },
  [ACTION.USER_BULK_DELETE_ACTION] ({ commit, dispatch }) {
    const ids = getIdsFromUrl(path(['history', 'current', 'query'], router))
    commit(SET_USER_LIST, [])
    return axios.delete(API.USER_BULK_DELETE, { data: { ids } }).then(() => {
      dispatch(ACTION.NOTIFY_ACTION, {
        content: i18n.t('Successfully deleted'),
        value: true,
        color: 'success'
      }, { root: true })
    }).catch(() => {
      dispatch(ACTION.NOTIFY_ACTION, {
        content: i18n.t('Error on deleting'),
        value: true,
        color: 'error'
      }, { root: true })
    }).finally(() => {
      dispatch(ACTION.GET_USER_LIST_ACTION)
    })
  }
}
