/* ============
 * Mutations for the organization state
 * ============
 *
 * The mutations that are available on the
 * organization state.
 */

import {
  SET_ORGANIZATION_DETAIL,
  SET_ORGANIZATION_LIST
} from './mutation-types'

/* eslint-disable no-param-reassign */
export default {
  [SET_ORGANIZATION_LIST] (state, list) {
    state.list = list
  },
  [SET_ORGANIZATION_DETAIL] (state, detail) {
    state.detail = detail
  }
}
