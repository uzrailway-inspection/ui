/* ============
 * Mutation types for the organization state
 * ============
 *
 */

export const SET_ORGANIZATION_LIST = 'SET_ORGANIZATION_LIST'
export const SET_ORGANIZATION_DETAIL = 'SET_ORGANIZATION_DETAIL'

export default {
  SET_ORGANIZATION_LIST,
  SET_ORGANIZATION_DETAIL
}
