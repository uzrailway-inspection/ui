/* ============
 * Actions for the organization state
 * ============
 *
 */

import path from 'ramda/src/path'
import { sprintf } from 'sprintf-js'
import router from '@/plugins/vue-router'
import axios from '@/plugins/axios'
import i18n from '@/plugins/vue-i18n'
import toSnakeCase from '@/helpers/toSnakeCase'
import * as ACTION from '@/constants/actionTypes'
import * as API from '@/constants/api'
import { SET_ORGANIZATION_LIST, SET_ORGANIZATION_DETAIL } from './mutation-types'
import { getIdsFromUrl } from '@/helpers/objects'

export default {
  [ACTION.GET_ORGANIZATION_LIST_ACTION] ({ commit }) {
    const params = path(['history', 'current', 'query'], router)
    commit(SET_ORGANIZATION_LIST, [])
    return axios.get(API.GET_ORGANIZATION_LIST, { params })
      .then(response => {
        commit(SET_ORGANIZATION_LIST, response.data)
      })
  },
  [ACTION.ORGANIZATION_CREATE_ACTION] ({ commit }, form) {
    return axios.post(API.ORGANIZATION_CREATE, toSnakeCase(form))
  },
  [ACTION.ORGANIZATION_DETAIL_ACTION] ({ commit }, id) {
    const url = sprintf(API.ORGANIZATION_DETAIL, id)
    commit(SET_ORGANIZATION_DETAIL, {})
    return axios.get(url).then(response => {
      commit(SET_ORGANIZATION_DETAIL, response.data)
      return response
    })
  },
  [ACTION.ORGANIZATION_UPDATE_ACTION] ({ commit }, id) {
    const url = sprintf(API.ORGANIZATION_DETAIL, id)
    return (form) => axios.patch(url, toSnakeCase(form)).then(() => {
      commit(SET_ORGANIZATION_DETAIL, {})
    })
  },
  [ACTION.ORGANIZATION_DELETE_ACTION] ({ commit, dispatch }, id) {
    const url = sprintf(API.ORGANIZATION_DETAIL, id)
    commit(SET_ORGANIZATION_LIST, [])
    return axios.delete(url).then(() => {
      dispatch(ACTION.NOTIFY_ACTION, {
        content: i18n.t('Successfully deleted'),
        value: true,
        color: 'success'
      }, { root: true })
    }).catch(() => {
      dispatch(ACTION.NOTIFY_ACTION, {
        content: i18n.t('Error on deleting'),
        value: true,
        color: 'error'
      }, { root: true })
    }).finally(() => {
      dispatch(ACTION.GET_ORGANIZATION_LIST_ACTION)
    })
  },
  [ACTION.ORGANIZATION_BULK_DELETE_ACTION] ({ commit, dispatch }) {
    const ids = getIdsFromUrl(path(['history', 'current', 'query'], router))
    commit(SET_ORGANIZATION_LIST, [])
    return axios.delete(API.ORGANIZATION_BULK_DELETE, { data: { ids } }).then(() => {
      dispatch(ACTION.NOTIFY_ACTION, {
        content: i18n.t('Successfully deleted'),
        value: true,
        color: 'success'
      }, { root: true })
    }).catch(() => {
      dispatch(ACTION.NOTIFY_ACTION, {
        content: i18n.t('Error on deleting'),
        value: true,
        color: 'error'
      }, { root: true })
    }).finally(() => {
      dispatch(ACTION.GET_ORGANIZATION_LIST_ACTION)
    })
  }
}
