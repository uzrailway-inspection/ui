/* ============
 * Actions for the station state
 * ============
 *
 */

import path from 'ramda/src/path'
import prop from 'ramda/src/prop'
import { sprintf } from 'sprintf-js'
import router from '@/plugins/vue-router'
import axios from '@/plugins/axios'
import i18n from '@/plugins/vue-i18n'
import toSnakeCase from '@/helpers/toSnakeCase'
import * as ACTION from '@/constants/actionTypes'
import * as API from '@/constants/api'
import { SET_STATION_LIST, SET_STATION_DETAIL } from './mutation-types'
import { getIdsFromUrl } from '@/helpers/objects'

export default {
  [ACTION.GET_STATION_LIST_ACTION] ({ commit }) {
    const params = path(['history', 'current', 'query'], router)
    commit(SET_STATION_LIST, [])
    return axios.get(API.GET_STATION_LIST, { params })
      .then(response => {
        commit(SET_STATION_LIST, response.data)
      })
  },
  [ACTION.STATION_CREATE_ACTION] ({ commit }, form) {
    const data = toSnakeCase({
      ...form,
      district: prop('id', form.district)
    })
    return axios.post(API.STATION_CREATE, data)
  },
  [ACTION.STATION_DETAIL_ACTION] ({ commit }, id) {
    const url = sprintf(API.STATION_DETAIL, id)
    commit(SET_STATION_DETAIL, {})
    return axios.get(url).then(response => {
      commit(SET_STATION_DETAIL, response.data)
      return response
    })
  },
  [ACTION.STATION_UPDATE_ACTION] ({ commit }, id) {
    const url = sprintf(API.STATION_DETAIL, id)
    return (form) => {
      const data = toSnakeCase({
        ...form,
        district: prop('id', form.district)
      })
      return axios.patch(url, data).then(() => {
        commit(SET_STATION_DETAIL, {})
      })
    }
  },
  [ACTION.STATION_DELETE_ACTION] ({ commit, dispatch }, id) {
    const url = sprintf(API.STATION_DETAIL, id)
    commit(SET_STATION_LIST, [])
    return axios.delete(url).then(() => {
      dispatch(ACTION.NOTIFY_ACTION, {
        content: i18n.t('Successfully deleted'),
        value: true,
        color: 'success'
      }, { root: true })
    }).catch(() => {
      dispatch(ACTION.NOTIFY_ACTION, {
        content: i18n.t('Error on deleting'),
        value: true,
        color: 'error'
      }, { root: true })
    }).finally(() => {
      dispatch(ACTION.GET_STATION_LIST_ACTION)
    })
  },
  [ACTION.STATION_BULK_DELETE_ACTION] ({ commit, dispatch }) {
    const ids = getIdsFromUrl(path(['history', 'current', 'query'], router))
    commit(SET_STATION_LIST, [])
    return axios.delete(API.STATION_BULK_DELETE, { data: { ids } }).then(() => {
      dispatch(ACTION.NOTIFY_ACTION, {
        content: i18n.t('Successfully deleted'),
        value: true,
        color: 'success'
      }, { root: true })
    }).catch(() => {
      dispatch(ACTION.NOTIFY_ACTION, {
        content: i18n.t('Error on deleting'),
        value: true,
        color: 'error'
      }, { root: true })
    }).finally(() => {
      dispatch(ACTION.GET_STATION_LIST_ACTION)
    })
  }
}
