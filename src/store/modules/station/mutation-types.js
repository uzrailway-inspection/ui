/* ============
 * Mutation types for the station state
 * ============
 *
 */

export const SET_STATION_LIST = 'SET_STATION_LIST'
export const SET_STATION_DETAIL = 'SET_STATION_DETAIL'

export default {
  SET_STATION_LIST,
  SET_STATION_DETAIL
}
