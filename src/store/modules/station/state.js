/* ============
 * State of the station module
 * ============
 *
 * The initial state of the station module
 */

export default {
  list: {},
  detail: null
}
