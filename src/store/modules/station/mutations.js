/* ============
 * Mutations for the station state
 * ============
 *
 * The mutations that are available on the
 * station state.
 */

import {
  SET_STATION_LIST,
  SET_STATION_DETAIL
} from './mutation-types'

/* eslint-disable no-param-reassign */
export default {
  [SET_STATION_LIST] (state, list) {
    state.list = list
  },
  [SET_STATION_DETAIL] (state, detail) {
    state.detail = detail
  }
}
