/* ============
 * Mutations for the company state
 * ============
 *
 * The mutations that are available on the
 * company state.
 */

import {
  SET_COMPANY_LIST,
  SET_COMPANY_DETAIL
} from './mutation-types'

/* eslint-disable no-param-reassign */
export default {
  [SET_COMPANY_LIST] (state, list) {
    state.list = list
  },
  [SET_COMPANY_DETAIL] (state, detail) {
    state.detail = detail
  }
}
