/* ============
 * Mutation types for the company state
 * ============
 *
 */

export const SET_COMPANY_LIST = 'SET_COMPANY_LIST'
export const SET_COMPANY_DETAIL = 'SET_COMPANY_DETAIL'

export default {
  SET_COMPANY_LIST,
  SET_COMPANY_DETAIL
}
