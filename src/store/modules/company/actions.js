/* ============
 * Actions for the company state
 * ============
 *
 */

import path from 'ramda/src/path'
import find from 'ramda/src/find'
import pathEq from 'ramda/src/pathEq'
import compose from 'ramda/src/compose'
import prop from 'ramda/src/prop'
import equals from 'ramda/src/equals'
import all from 'ramda/src/all'
import not from 'ramda/src/not'
import filter from 'ramda/src/filter'
import map from 'ramda/src/map'
import { sprintf } from 'sprintf-js'
import axios from '@/plugins/axios'
import router from '@/plugins/vue-router'
import i18n from '@/plugins/vue-i18n'
import toSnakeCase from '@/helpers/toSnakeCase'
import * as ACTION from '@/constants/actionTypes'
import * as API from '@/constants/api'
import { SET_COMPANY_LIST, SET_COMPANY_DETAIL } from './mutation-types'
import { getIdsFromUrl } from '@/helpers/objects'

export default {
  [ACTION.GET_COMPANY_LIST_ACTION] ({ commit }) {
    const params = path(['history', 'current', 'query'], router)
    commit(SET_COMPANY_LIST, [])
    return axios.get(API.GET_COMPANY_LIST, { params })
      .then(response => {
        commit(SET_COMPANY_LIST, response.data)
      })
  },
  [ACTION.COMPANY_CREATE_ACTION] ({ commit }, form) {
    const data = toSnakeCase({
      ...form,
      factorValues: compose(
        map(item => ({ value: item.value, factor: item.factor.id })),
        filter(prop('value'))
      )(form.factorValues),
      station: prop('id', form.station),
      mainOrganization: prop('id', form.mainOrganization),
      district: prop('id', form.district)
    })
    return axios.post(API.COMPANY_CREATE, toSnakeCase(data))
  },
  [ACTION.COMPANY_DETAIL_ACTION] ({ commit }, id) {
    const url = sprintf(API.COMPANY_DETAIL, id)
    commit(SET_COMPANY_DETAIL, {})
    return axios.get(url).then(response => {
      commit(SET_COMPANY_DETAIL, response.data)
      return response
    })
  },
  [ACTION.COMPANY_UPDATE_ACTION] ({ commit, state }, id) {
    const url = sprintf(API.COMPANY_DETAIL, id)
    return (form) => {
      const factorValues = compose(
        map(item => ({ value: parseFloat(item.value), factor: item.factor.id })),
        filter(prop('value'))
      )(form.factorValues)
      const isFactorChanged = factorValues.length !== state.detail.factorValues.length || not(all(item => {
        return compose(
          equals(item.value),
          prop('value'),
          find(pathEq(['factor', 'id'], item.factor))
        )(state.detail.factorValues)
      }, factorValues))
      const data = toSnakeCase({
        ...form,
        station: prop('id', form.station),
        mainOrganization: prop('id', form.mainOrganization),
        district: prop('id', form.district),
        isFactorChanged,
        factorValues
      })
      return axios.patch(url, data).then(() => {
        commit(SET_COMPANY_DETAIL, {})
      })
    }
  },
  [ACTION.COMPANY_DELETE_ACTION] ({ commit, dispatch }, id) {
    const url = sprintf(API.COMPANY_DETAIL, id)
    commit(SET_COMPANY_LIST, [])
    return axios.delete(url).then(() => {
      dispatch(ACTION.NOTIFY_ACTION, {
        content: i18n.t('Successfully deleted'),
        value: true,
        color: 'success'
      }, { root: true })
    }).catch(() => {
      dispatch(ACTION.NOTIFY_ACTION, {
        content: i18n.t('Error on deleting'),
        value: true,
        color: 'error'
      }, { root: true })
    }).finally(() => {
      dispatch(ACTION.GET_COMPANY_LIST_ACTION)
    })
  },
  [ACTION.COMPANY_BULK_DELETE_ACTION] ({ commit, dispatch }) {
    const ids = getIdsFromUrl(path(['history', 'current', 'query'], router))
    commit(SET_COMPANY_LIST, [])
    return axios.delete(API.COMPANY_BULK_DELETE, { data: { ids } }).then(() => {
      dispatch(ACTION.NOTIFY_ACTION, {
        content: i18n.t('Successfully deleted'),
        value: true,
        color: 'success'
      }, { root: true })
    }).catch(() => {
      dispatch(ACTION.NOTIFY_ACTION, {
        content: i18n.t('Error on deleting'),
        value: true,
        color: 'error'
      }, { root: true })
    }).finally(() => {
      dispatch(ACTION.GET_COMPANY_LIST_ACTION)
    })
  }
}
