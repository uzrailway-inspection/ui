/* ============
 * Actions for the factor state
 * ============
 *
 */

import path from 'ramda/src/path'
import { sprintf } from 'sprintf-js'
import router from '@/plugins/vue-router'
import axios from '@/plugins/axios'
import i18n from '@/plugins/vue-i18n'
import toSnakeCase from '@/helpers/toSnakeCase'
import * as ACTION from '@/constants/actionTypes'
import * as API from '@/constants/api'
import { SET_FACTOR_LIST, SET_FACTOR_DETAIL } from './mutation-types'
import { getIdsFromUrl } from '@/helpers/objects'

export default {
  [ACTION.GET_FACTOR_LIST_ACTION] ({ commit }) {
    const params = path(['history', 'current', 'query'], router)
    commit(SET_FACTOR_LIST, [])
    return axios.get(API.GET_FACTOR_LIST, { params })
      .then(response => {
        commit(SET_FACTOR_LIST, response.data)
      })
  },
  [ACTION.FACTOR_CREATE_ACTION] ({ commit }, form) {
    return axios.post(API.FACTOR_CREATE, toSnakeCase(form))
  },
  [ACTION.FACTOR_DETAIL_ACTION] ({ commit }, id) {
    const url = sprintf(API.FACTOR_DETAIL, id)
    commit(SET_FACTOR_DETAIL, {})
    return axios.get(url).then(response => {
      commit(SET_FACTOR_DETAIL, response.data)
      return response
    })
  },
  [ACTION.CHANGE_FACTOR_POSITION_ACTION] ({ commit, dispatch }, id) {
    const url = sprintf(API.CHANGE_FACTOR_POSITION, id)
    return (replaceId) => axios.post(url, { replace_id: replaceId })
  },
  [ACTION.FACTOR_UPDATE_ACTION] ({ commit }, id) {
    const url = sprintf(API.FACTOR_DETAIL, id)
    return (form) => axios.patch(url, toSnakeCase(form)).then(() => {
      commit(SET_FACTOR_DETAIL, {})
    })
  },
  [ACTION.FACTOR_DELETE_ACTION] ({ commit, dispatch }, id) {
    const url = sprintf(API.FACTOR_DETAIL, id)
    commit(SET_FACTOR_LIST, [])
    return axios.delete(url).then(() => {
      dispatch(ACTION.NOTIFY_ACTION, {
        content: i18n.t('Successfully deleted'),
        value: true,
        color: 'success'
      }, { root: true })
    }).catch(() => {
      dispatch(ACTION.NOTIFY_ACTION, {
        content: i18n.t('Error on deleting'),
        value: true,
        color: 'error'
      }, { root: true })
    }).finally(() => {
      dispatch(ACTION.GET_FACTOR_LIST_ACTION)
    })
  },
  [ACTION.FACTOR_BULK_DELETE_ACTION] ({ commit, dispatch }) {
    const ids = getIdsFromUrl(path(['history', 'current', 'query'], router))
    commit(SET_FACTOR_LIST, [])
    return axios.delete(API.FACTOR_BULK_DELETE, { data: { ids } }).then(() => {
      dispatch(ACTION.NOTIFY_ACTION, {
        content: i18n.t('Successfully deleted'),
        value: true,
        color: 'success'
      }, { root: true })
    }).catch(() => {
      dispatch(ACTION.NOTIFY_ACTION, {
        content: i18n.t('Error on deleting'),
        value: true,
        color: 'error'
      }, { root: true })
    }).finally(() => {
      dispatch(ACTION.GET_FACTOR_LIST_ACTION)
    })
  }
}
