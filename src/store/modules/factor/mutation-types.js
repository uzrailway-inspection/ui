/* ============
 * Mutation types for the factor state
 * ============
 *
 */

export const SET_FACTOR_LIST = 'SET_FACTOR_LIST'
export const SET_FACTOR_DETAIL = 'SET_FACTOR_DETAIL'

export default {
  SET_FACTOR_LIST,
  SET_FACTOR_DETAIL
}
