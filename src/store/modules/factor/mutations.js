/* ============
 * Mutations for the factor state
 * ============
 *
 * The mutations that are available on the
 * factor state.
 */

import {
  SET_FACTOR_LIST,
  SET_FACTOR_DETAIL
} from './mutation-types'

/* eslint-disable no-param-reassign */
export default {
  [SET_FACTOR_LIST] (state, list) {
    state.list = list
  },
  [SET_FACTOR_DETAIL] (state, detail) {
    state.detail = detail
  }
}
