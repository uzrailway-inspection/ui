/* ============
 * Actions for the rating state
 * ============
 *
 */

import path from 'ramda/src/path'
import * as ACTION from '@/constants/actionTypes'
import * as API from '@/constants/api'
import axios from '@/plugins/axios'
import router from '@/plugins/vue-router'
import { SET_REPORT_LIST } from './mutation-types'
import { sprintf } from 'sprintf-js'

export default {
  [ACTION.GET_REPORT_LIST_ACTION] ({ commit }) {
    const params = path(['history', 'current', 'query'], router)
    commit(SET_REPORT_LIST, [])
    return axios.get(API.GET_REPORT_LIST, { params })
      .then(response => {
        commit(SET_REPORT_LIST, response.data)
      })
  },
  [ACTION.DOWNLOAD_REPORT_ACTION] ({ commit }, id) {
    const url = sprintf(API.DOWNLOAD_REPORT, id)
    return (config) => axios.get(url, config)
  }
}
