/* ============
 * Mutations for the report state
 * ============
 *
 * The mutations that are available on the
 * report state.
 */

import {
  SET_REPORT_LIST
} from './mutation-types'

/* eslint-disable no-param-reassign */
export default {
  [SET_REPORT_LIST] (state, list) {
    state.list = list
  }
}
