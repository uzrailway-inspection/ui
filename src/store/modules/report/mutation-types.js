/* ============
 * Mutation types for the report state
 * ============
 *
 */

export const SET_REPORT_LIST = 'SET_REPORT_LIST'

export default {
  SET_REPORT_LIST
}
