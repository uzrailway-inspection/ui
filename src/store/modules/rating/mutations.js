/* ============
 * Mutations for the rating state
 * ============
 *
 * The mutations that are available on the
 * rating state.
 */

import {
  SET_RATING_LIST,
  SET_RATING_DETAIL,
  SET_RATING_STATISTIC,
  SET_RATING_FULL_STATISTIC
} from './mutation-types'

/* eslint-disable no-param-reassign */
export default {
  [SET_RATING_LIST] (state, list) {
    state.list = list
  },
  [SET_RATING_DETAIL] (state, detail) {
    state.detail = detail
  },
  [SET_RATING_STATISTIC] (state, statistic) {
    state.statistic = statistic
  },
  [SET_RATING_FULL_STATISTIC] (state, fullStatistic) {
    state.fullStatistic = fullStatistic
  }
}
