/* ============
 * State of the rating module
 * ============
 *
 * The initial state of the rating module
 */

export default {
  list: {},
  detail: null,
  statistic: [],
  fullStatistic: []
}
