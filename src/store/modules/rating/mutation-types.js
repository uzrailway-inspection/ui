/* ============
 * Mutation types for the rating state
 * ============
 *
 */

export const SET_RATING_LIST = 'SET_RATING_LIST'
export const SET_RATING_DETAIL = 'SET_RATING_DETAIL'
export const SET_RATING_STATISTIC = 'SET_RATING_STATISTIC'
export const SET_RATING_FULL_STATISTIC = 'SET_RATING_FULL_STATISTIC'

export default {
  SET_RATING_LIST,
  SET_RATING_DETAIL,
  SET_RATING_STATISTIC,
  SET_RATING_FULL_STATISTIC
}
