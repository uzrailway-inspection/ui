/* ============
 * Actions for the rating state
 * ============
 *
 */

import path from 'ramda/src/path'
import pathEq from 'ramda/src/pathEq'
import find from 'ramda/src/find'
import * as ACTION from '@/constants/actionTypes'
import * as API from '@/constants/api'
import axios from '@/plugins/axios'
import router from '@/plugins/vue-router'
import { SET_RATING_LIST, SET_RATING_DETAIL, SET_RATING_STATISTIC, SET_RATING_FULL_STATISTIC } from './mutation-types'
import { sprintf } from 'sprintf-js'
import { getIdsFromUrl } from '@/helpers/objects'
import i18n from '@/plugins/vue-i18n'

export default {
  [ACTION.GET_RATING_LIST_ACTION] ({ commit }) {
    const params = path(['history', 'current', 'query'], router)
    commit(SET_RATING_LIST, [])
    return axios.get(API.GET_RATING_LIST, { params })
      .then(response => {
        commit(SET_RATING_LIST, response.data)
      })
  },
  [ACTION.GET_RATING_FOR_INSPECTOR_LIST_ACTION] ({ commit }) {
    const params = path(['history', 'current', 'query'], router)
    commit(SET_RATING_LIST, [])
    return axios.get(API.GET_RATING_FOR_INSPECTOR_LIST, { params })
      .then(response => {
        commit(SET_RATING_LIST, response.data)
      })
  },
  [ACTION.RATING_DETAIL_ACTION] ({ commit, dispatch }, id) {
    const url = sprintf(API.RATING_DETAIL, id)
    return axios.get(url).then(response => {
      const rating = response.data
      const factorValues = rating.factorValues
      axios.get(API.GET_FACTOR_LIST, { params: { page_size: 100 } }).then(res => {
        const factors = res.data.results
        factors.forEach((factor) => {
          const hasFactor = find(pathEq(['factor', 'id'], factor.id), factorValues)
          if (!hasFactor) {
            factorValues.push({ value: null, factor })
          }
        })
        rating.factorValues = factorValues
        commit(SET_RATING_DETAIL, rating)
      })
    })
  },
  [ACTION.GET_RATING_STATISTIC_ACTION] ({ commit, dispatch }, id) {
    const url = sprintf(API.GET_RATING_STATISTIC, id)
    return axios.get(url).then(response => {
      commit(SET_RATING_STATISTIC, response.data)
    })
  },
  [ACTION.GET_RATING_FULL_STATISTIC_ACTION] ({ commit, dispatch }, id) {
    const url = sprintf(API.GET_RATING_FULL_STATISTIC, id)
    return (year) => axios.get(url, { params: { year } }).then(response => {
      commit(SET_RATING_FULL_STATISTIC, response.data)
    })
  },
  [ACTION.EXPORT_REPORT_ACTION] ({ dispatch }) {
    const ids = getIdsFromUrl(path(['history', 'current', 'query'], router))
    return axios.post(API.REPORT_CREATE, { ids }).then((response) => {
      dispatch(ACTION.NOTIFY_ACTION, {
        content: i18n.t('Successfully exported'),
        value: true,
        color: 'success'
      }, { root: true })
      dispatch(`${ACTION.REPORT}/${ACTION.DOWNLOAD_REPORT_ACTION}`, response.data.id, { root: true }).then(download => {
        download({
          responseType: 'blob'
        }).then((res) => {
          const url = URL.createObjectURL(res.request.response)
          window.open(url, '__blank')
          setTimeout(() => {
            URL.revokeObjectURL(url)
          }, 2000)
        })
      })
      router.push()
    }).catch(() => {
      dispatch(ACTION.NOTIFY_ACTION, {
        content: i18n.t('Error on exporting'),
        value: true,
        color: 'error'
      }, { root: true })
    })
  },
  [ACTION.REVISION_CONFIRM_ACTION] ({ commit, dispatch }, id) {
    return dispatch(`${ACTION.REVISION}/${ACTION.REVISION_CONFIRM_ACTION}`, id, { root: true })
  }
}
