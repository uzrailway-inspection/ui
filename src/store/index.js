/* ============
 * Vuex Store
 * ============
 *
 * The store of the application.
 *
 * http://vuex.vuejs.org/en/index.html
 */

import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger'

// Namespaces
import * as NAMESPACE from '@/constants/actionTypes'

// Modules
import general from './modules/general'
import company from './modules/company'
import organization from './modules/organization'
import region from './modules/region'
import district from './modules/district'
import category from './modules/category'
import station from './modules/station'
import factor from './modules/factor'
import rating from './modules/rating'
import user from './modules/user'
import report from './modules/report'
import revision from './modules/revision'
import userGroup from './modules/user-group'

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  ...general,

  modules: {
    [NAMESPACE.COMPANY]: company,
    [NAMESPACE.ORGANIZATION]: organization,
    [NAMESPACE.REGION]: region,
    [NAMESPACE.DISTRICT]: district,
    [NAMESPACE.CATEGORY]: category,
    [NAMESPACE.STATION]: station,
    [NAMESPACE.FACTOR]: factor,
    [NAMESPACE.RATING]: rating,
    [NAMESPACE.REPORT]: report,
    [NAMESPACE.USER]: user,
    [NAMESPACE.REVISION]: revision,
    [NAMESPACE.USER_GROUP]: userGroup
  },

  /**
   * If strict mode should be enabled.
   */
  strict: debug,
  devtools: debug,

  /**
   * Plugins used in the store.
   */
  plugins: debug ? [createLogger()] : []
})
