/* ============
 * Revision routes
 * ============
 *
 */

import * as PAGE from '@/views/common'
import CommonRoot from '@/views/common/Common'
import * as ROUTE from '@/constants/routes'

export default [
  {
    path: ROUTE.REVISION_PATH,
    component: CommonRoot,
    children: [
      {
        path: ROUTE.LIST,
        name: ROUTE.REVISION_LIST,
        component: () => PAGE.RevisionList,
        meta: {
          breadcrumb: 'Revision list',
          permissions: 'list_revision'
        }
      },
      {
        path: ROUTE.CREATE,
        name: ROUTE.REVISION_CREATE,
        component: () => PAGE.RevisionCreate,
        meta: {
          breadcrumb: 'Revision create',
          permissions: 'create_revision'
        }
      }
    ]
  }
]
