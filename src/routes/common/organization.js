/* ============
 * Organization routes
 * ============
 *
 */

import * as PAGE from '@/views/common'
import CommonRoot from '@/views/common/Common'
import * as ROUTE from '@/constants/routes'

export default [
  {
    path: ROUTE.ORGANIZATION_PATH,
    component: CommonRoot,
    children: [
      {
        path: ROUTE.LIST,
        name: ROUTE.ORGANIZATION_LIST,
        component: () => PAGE.OrganizationList,
        meta: {
          breadcrumb: 'Organization list',
          permissions: 'list_mainorganization'
        }
      },
      {
        path: ROUTE.CREATE,
        name: ROUTE.ORGANIZATION_CREATE,
        component: () => PAGE.OrganizationCreate,
        meta: {
          breadcrumb: 'Organization create',
          permissions: 'create_mainorganization'
        }
      },
      {
        path: ROUTE.UPDATE,
        name: ROUTE.ORGANIZATION_UPDATE,
        component: () => PAGE.OrganizationUpdate,
        meta: {
          breadcrumb: 'Organization update',
          permissions: 'update_mainorganization'
        }
      }
    ]
  }
]
