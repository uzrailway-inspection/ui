/* ============
 * Factor routes
 * ============
 *
 */

import * as PAGE from '@/views/common'
import CommonRoot from '@/views/common/Common'
import * as ROUTE from '@/constants/routes'

export default [
  {
    path: ROUTE.FACTOR_PATH,
    component: CommonRoot,
    children: [
      {
        path: ROUTE.LIST,
        name: ROUTE.FACTOR_LIST,
        component: () => PAGE.FactorList,
        meta: {
          breadcrumb: 'Factor list',
          permissions: 'list_factor'
        }
      },
      {
        path: ROUTE.CREATE,
        name: ROUTE.FACTOR_CREATE,
        component: () => PAGE.FactorCreate,
        meta: {
          breadcrumb: 'Factor create',
          permissions: 'create_factor'
        }
      },
      {
        path: ROUTE.UPDATE,
        name: ROUTE.FACTOR_UPDATE,
        component: () => PAGE.FactorUpdate,
        meta: {
          breadcrumb: 'Factor update',
          permissions: 'update_factor'
        }
      }
    ]
  }
]
