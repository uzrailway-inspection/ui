/* ============
 * District routes
 * ============
 *
 */

import * as PAGE from '@/views/common'
import CommonRoot from '@/views/common/Common'
import * as ROUTE from '@/constants/routes'

export default [
  {
    path: ROUTE.DISTRICT_PATH,
    component: CommonRoot,
    children: [
      {
        path: ROUTE.LIST,
        name: ROUTE.DISTRICT_LIST,
        component: () => PAGE.DistrictList,
        meta: {
          breadcrumb: 'District list',
          permissions: 'list_district'
        }
      },
      {
        path: ROUTE.CREATE,
        name: ROUTE.DISTRICT_CREATE,
        component: () => PAGE.DistrictCreate,
        meta: {
          breadcrumb: 'Company create',
          permissions: 'create_district'
        }
      },
      {
        path: ROUTE.UPDATE,
        name: ROUTE.DISTRICT_UPDATE,
        component: () => PAGE.DistrictUpdate,
        meta: {
          breadcrumb: 'District update',
          permissions: 'update_district'
        }
      }
    ]
  }
]
