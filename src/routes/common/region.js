/* ============
 * Region routes
 * ============
 *
 */

import * as PAGE from '@/views/common'
import CommonRoot from '@/views/common/Common'
import * as ROUTE from '@/constants/routes'

export default [
  {
    path: ROUTE.REGION_PATH,
    component: CommonRoot,
    children: [
      {
        path: ROUTE.LIST,
        name: ROUTE.REGION_LIST,
        component: () => PAGE.RegionList,
        meta: {
          breadcrumb: 'Region list',
          permissions: 'list_region'
        }
      },
      {
        path: ROUTE.CREATE,
        name: ROUTE.REGION_CREATE,
        component: () => PAGE.RegionCreate,
        meta: {
          breadcrumb: 'Region create',
          permissions: 'create_region'
        }
      },
      {
        path: ROUTE.UPDATE,
        name: ROUTE.REGION_UPDATE,
        component: () => PAGE.RegionUpdate,
        meta: {
          breadcrumb: 'Region update',
          permissions: 'update_region'
        }
      }
    ]
  }
]
