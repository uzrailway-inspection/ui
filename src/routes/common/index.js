/* ============
 * Common routes
 * ============
 *
 */
import RouterView from '@/components/RouterView'

import Company from './company'
import Category from './category'
import District from './district'
import Factor from './factor'
import Organization from './organization'
import Region from './region'
import Station from './station'
import Revision from './revision'
import * as ROUTE from '@/constants/routes'

export default [
  {
    path: ROUTE.COMMON_PATH,
    component: RouterView,
    children: [
      ...Company,
      ...Category,
      ...District,
      ...Factor,
      ...Organization,
      ...Region,
      ...Station,
      ...Revision
    ]
  }
]
