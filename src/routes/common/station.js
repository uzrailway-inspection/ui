/* ============
 * Station routes
 * ============
 *
 */

import * as PAGE from '@/views/common'
import CommonRoot from '@/views/common/Common'
import * as ROUTE from '@/constants/routes'

export default [
  {
    path: ROUTE.STATION_PATH,
    component: CommonRoot,
    children: [
      {
        path: ROUTE.LIST,
        name: ROUTE.STATION_LIST,
        component: () => PAGE.StationList,
        meta: {
          breadcrumb: 'Station list',
          permissions: 'list_station'
        }
      },
      {
        path: ROUTE.CREATE,
        name: ROUTE.STATION_CREATE,
        component: () => PAGE.StationCreate,
        meta: {
          breadcrumb: 'Station create',
          permissions: 'create_station'
        }
      },
      {
        path: ROUTE.UPDATE,
        name: ROUTE.STATION_UPDATE,
        component: () => PAGE.StationUpdate,
        meta: {
          breadcrumb: 'Station update',
          permissions: 'update_station'
        }
      }
    ]
  }
]
