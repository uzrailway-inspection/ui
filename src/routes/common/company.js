/* ============
 * Company routes
 * ============
 *
 */

import * as PAGE from '@/views/common'
import CommonRoot from '@/views/common/Common'
import * as ROUTE from '@/constants/routes'

export default [
  {
    path: ROUTE.COMPANY_PATH,
    component: CommonRoot,
    children: [
      {
        path: ROUTE.LIST,
        name: ROUTE.COMPANY_LIST,
        component: () => PAGE.CompanyList,
        meta: {
          breadcrumb: 'Company list',
          permissions: 'list_company'
        }
      },
      {
        path: ROUTE.CREATE,
        name: ROUTE.COMPANY_CREATE,
        component: () => PAGE.CompanyCreate,
        meta: {
          breadcrumb: 'Company create',
          permissions: 'create_company'
        }
      },
      {
        path: ROUTE.UPDATE,
        name: ROUTE.COMPANY_UPDATE,
        component: () => PAGE.CompanyUpdate,
        meta: {
          breadcrumb: 'Company update',
          permissions: 'update_company'
        }
      }
    ]
  }
]
