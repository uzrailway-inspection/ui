/* ============
 * Category routes
 * ============
 *
 */

import * as PAGE from '@/views/common'
import CommonRoot from '@/views/common/Common'
import * as ROUTE from '@/constants/routes'

export default [
  {
    path: ROUTE.CATEGORY_PATH,
    component: CommonRoot,
    children: [
      {
        path: ROUTE.LIST,
        name: ROUTE.CATEGORY_LIST,
        component: () => PAGE.CategoryList,
        meta: {
          breadcrumb: 'Category list',
          permissions: 'list_category'
        }
      },
      {
        path: ROUTE.CREATE,
        name: ROUTE.CATEGORY_CREATE,
        component: () => PAGE.CategoryCreate,
        meta: {
          breadcrumb: 'Category create',
          permissions: 'create_category'
        }
      },
      {
        path: ROUTE.UPDATE,
        name: ROUTE.CATEGORY_UPDATE,
        component: () => PAGE.CategoryUpdate,
        meta: {
          breadcrumb: 'Category update',
          permissions: 'update_category'
        }
      }
    ]
  }
]
