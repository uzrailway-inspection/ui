/* ============
 * Rating routes
 * ============
 *
 */
import RouterView from '@/components/RouterView'

import * as PAGE from '@/views/rating'
import * as ROUTE from '@/constants/routes'

export default [
  {
    path: ROUTE.RATING_PATH,
    component: RouterView,
    children: [
      {
        path: ROUTE.LIST,
        name: ROUTE.RATING_LIST,
        component: () => PAGE.RatingListRoot,
        meta: {
          breadcrumb: 'Rating list',
          permissions: ['list_rating', 'get_rating_for_inspector_rating']
        }
      }
    ]
  }
]
