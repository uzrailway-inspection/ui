/* ============
 * Instruction routes
 * ============
 *
 */
import RouterView from '@/components/RouterView'

import * as PAGE from '@/views/instruction'
import * as ROUTE from '@/constants/routes'

export default [
  {
    path: ROUTE.INSTRUCTION_PATH,
    component: RouterView,
    children: [
      {
        path: ROUTE.LIST,
        name: ROUTE.INSTRUCTION,
        component: () => PAGE.Instruction,
        meta: {
          breadcrumb: 'Instruction'
        }
      }
    ]
  }
]
