/* ============
 * Routes File
 * ============
 *
 */
import AdminLayout from '@/components/layouts/AdminLayout'
import * as ROUTE from '@/constants/routes'
import Users from './user'

import Rating from './rating'
import Instruction from './instruction'
import Report from './report'
import Common from './common'

import store from '@/store'
import i18n from '@/plugins/vue-i18n'
import RouterView from '@/components/RouterView'
import { GET_ME_ACTION } from '@/constants/actionTypes'

const Login = import(/* webpackChunkName: "login" */'@/views/login/Login')

export default [
  {
    path: ROUTE.MAIN_ROOT_PATH,
    component: RouterView,
    children: [
      {
        path: '',
        component: AdminLayout,
        meta: {
          auth: true
        },
        beforeEnter: (to, from, next) => {
          const { me } = store.state
          if (!me) {
            return store.dispatch(GET_ME_ACTION).then(() => {
              next()
            })
          }
          return next()
        },
        children: [
          ...Users,
          ...Rating,
          ...Instruction,
          ...Common,
          ...Report
        ]
      },
      {
        name: ROUTE.N_LOGIN,
        path: ROUTE.LOGIN_PATH,
        component: () => Login,
        beforeEnter: (to, from, next) => {
          const token = store.getters.getToken
          if (token) {
            return next('/')
          }
          return next()
        }
      }
    ]
  },
  {
    path: '/*',
    beforeEnter: (to, from, next) => {
      const lang = i18n.locale
      return next('/' + lang)
    }
  }
]
