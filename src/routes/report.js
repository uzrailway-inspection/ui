/* ============
 * Report routes
 * ============
 *
 */
import RouterView from '@/components/RouterView'

import * as PAGE from '@/views/report'
import * as ROUTE from '@/constants/routes'

export default [
  {
    path: ROUTE.REPORT_PATH,
    component: RouterView,
    children: [
      {
        path: ROUTE.LIST,
        name: ROUTE.REPORT_LIST,
        component: () => PAGE.ReportList,
        meta: {
          breadcrumb: 'Report list',
          permissions: 'list_report'
        }
      }
    ]
  }
]
