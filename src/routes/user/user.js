/* ============
 * Users routes
 * ============
 *
 */

import UsersRoot from '@/views/user/Users'
import * as PAGE from '@/views/user'
import * as ROUTE from '@/constants/routes'

export default [
  {
    path: ROUTE.USERS_PATH,
    component: UsersRoot,
    children: [
      {
        path: ROUTE.LIST,
        name: ROUTE.USER_LIST,
        component: () => PAGE.UserList,
        meta: {
          breadcrumb: 'User list',
          permissions: 'list_user'
        }
      },
      {
        name: ROUTE.USER_UPDATE,
        path: ROUTE.UPDATE,
        component: () => PAGE.UserUpdate,
        meta: {
          breadcrumb: 'User update',
          permissions: 'change_user'
        }
      },
      {
        name: ROUTE.USER_CREATE,
        path: ROUTE.CREATE,
        component: () => PAGE.UserCreate,
        meta: {
          breadcrumb: 'User create',
          permissions: 'add_user'
        }
      }
    ]
  }
]
