/* ============
 * User routes
 * ============
 *
 */
import RouterView from '@/components/RouterView'

import User from './user'
import Group from './group'
import * as ROUTE from '@/constants/routes'

export default [
  {
    path: ROUTE.USER_PATH,
    component: RouterView,
    children: [
      ...User,
      ...Group
    ]
  }
]
