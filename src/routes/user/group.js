/* ============
 * Groups routes
 * ============
 *
 */

import UsersRoot from '@/views/user/Users'
import * as PAGE from '@/views/user'
import * as ROUTE from '@/constants/routes'

export default [
  {
    path: ROUTE.GROUPS_PATH,
    component: UsersRoot,
    children: [
      {
        path: ROUTE.LIST,
        name: ROUTE.GROUP_LIST,
        component: () => PAGE.GroupList,
        meta: {
          breadcrumb: 'Group list',
          permissions: 'view_group'
        }
      },
      {
        name: ROUTE.GROUP_UPDATE,
        path: ROUTE.UPDATE,
        component: () => PAGE.GroupUpdate,
        meta: {
          breadcrumb: 'Group update',
          permissions: 'change_group'
        }
      },
      {
        name: ROUTE.GROUP_CREATE,
        path: ROUTE.CREATE,
        component: () => PAGE.GroupCreate,
        meta: {
          breadcrumb: 'Group create',
          permissions: 'add_group'
        }
      }
    ]
  }
]
