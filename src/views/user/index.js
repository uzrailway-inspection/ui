const UserList = import(/* webpackChunkName: "users" */'./users/UserList')
const UserCreate = import(/* webpackChunkName: "users" */'./users/UserCreate')
const UserUpdate = import(/* webpackChunkName: "users" */'./users/UserUpdate')

const GroupList = import(/* webpackChunkName: "groups" */'./groups/GroupList')
const GroupCreate = import(/* webpackChunkName: "groups" */'./groups/GroupCreate')
const GroupUpdate = import(/* webpackChunkName: "groups" */'./groups/GroupUpdate')

export {
  UserList,
  UserCreate,
  UserUpdate,
  GroupList,
  GroupCreate,
  GroupUpdate
}
