const CompanyCreate = import(/* webpackChunkName: "company" */'./company/CompanyCreate')
const CompanyList = import(/* webpackChunkName: "company" */'./company/CompanyList')
const CompanyUpdate = import(/* webpackChunkName: "company" */'./company/CompanyUpdate')

const OrganizationCreate = import(/* webpackChunkName: "organization" */'./organization/OrganizationCreate')
const OrganizationList = import(/* webpackChunkName: "organization" */'./organization/OrganizationList')
const OrganizationUpdate = import(/* webpackChunkName: "organization" */'./organization/OrganizationUpdate')

const RegionCreate = import(/* webpackChunkName: "region" */'./region/RegionCreate')
const RegionList = import(/* webpackChunkName: "region" */'./region/RegionList')
const RegionUpdate = import(/* webpackChunkName: "region" */'./region/RegionUpdate')

const DistrictCreate = import(/* webpackChunkName: "district" */'./district/DistrictCreate')
const DistrictList = import(/* webpackChunkName: "district" */'./district/DistrictList')
const DistrictUpdate = import(/* webpackChunkName: "district" */'./district/DistrictUpdate')

const CategoryCreate = import(/* webpackChunkName: "category" */'./category/CategoryCreate')
const CategoryList = import(/* webpackChunkName: "category" */'./category/CategoryList')
const CategoryUpdate = import(/* webpackChunkName: "category" */'./category/CategoryUpdate')

const StationCreate = import(/* webpackChunkName: "station" */'./station/StationCreate')
const StationList = import(/* webpackChunkName: "station" */'./station/StationList')
const StationUpdate = import(/* webpackChunkName: "station" */'./station/StationUpdate')

const FactorCreate = import(/* webpackChunkName: "factor" */'./factor/FactorCreate')
const FactorList = import(/* webpackChunkName: "factor" */'./factor/FactorList')
const FactorUpdate = import(/* webpackChunkName: "factor" */'./factor/FactorUpdate')

const RevisionCreate = import(/* webpackChunkName: "revision" */'./revision/RevisionCreate')
const RevisionList = import(/* webpackChunkName: "revision" */'./revision/RevisionList')

export {
  RevisionCreate,
  RevisionList,
  FactorCreate,
  FactorList,
  FactorUpdate,
  StationCreate,
  StationList,
  StationUpdate,
  CategoryCreate,
  CategoryList,
  CategoryUpdate,
  OrganizationCreate,
  OrganizationList,
  OrganizationUpdate,
  RegionCreate,
  RegionList,
  RegionUpdate,
  DistrictCreate,
  DistrictList,
  DistrictUpdate,
  CompanyCreate,
  CompanyList,
  CompanyUpdate
}
