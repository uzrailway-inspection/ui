import i18n from '@/plugins/vue-i18n'
import lensIndex from 'ramda/src/lensIndex'
import over from 'ramda/src/over'
import __ from 'ramda/src/__'
import join from 'ramda/src/join'
import compose from 'ramda/src/compose'
import prepend from 'ramda/src/prepend'
import prop from 'ramda/src/prop'
import toUpper from 'ramda/src/toUpper'

export const translateObject = (key, obj) => {
  const lang = i18n.locale

  return compose(
    prop(__, obj),
    join(''),
    prepend(key),
    over(lensIndex(0), toUpper)
  )(lang)
}
