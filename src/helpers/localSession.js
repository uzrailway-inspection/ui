import is from 'ramda/src/is'

export const getSession =
  (key, defaultValue = null) => {
    const value = sessionStorage.getItem(key)
    if (value || is(Boolean, value)) {
      try {
        return JSON.parse(value)
      } catch (e) {}
    }
    return defaultValue
  }

export const setSession = (key, value) => {
  if ((value && key) || is(Boolean, value)) sessionStorage.setItem(key, JSON.stringify(value))
}

export const removeSession = key => {
  sessionStorage.removeItem(key)
}
