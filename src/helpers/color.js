import replace from 'ramda/src/replace'
import length from 'ramda/src/length'

export const hexToRgba = (hex, opacity) => {
  const ONE = 1
  const TWO = 2
  const THREE = 3

  const trimmedHex = replace('#', '', hex)
  const HEX = length(trimmedHex) === THREE ? `#${trimmedHex}${trimmedHex}` : hex
  const OPACITY = opacity || '1'
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(HEX)
  const R = parseInt(result[ONE], 16)
  const G = parseInt(result[TWO], 16)
  const B = parseInt(result[THREE], 16)
  const RGBA = `rgba(${R}, ${G}, ${B}, ${OPACITY})`
  return result ? RGBA : null
}
