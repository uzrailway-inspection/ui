import is from 'ramda/src/is'

export const getStorage = (key, defaultValue = null) => {
  const value = localStorage.getItem(key)
  if (value || is(Boolean, value)) {
    try {
      return JSON.parse(value)
    } catch (e) {}
  }
  return defaultValue
}

export const setStorage = (key, value) => {
  if ((value && key) || is(Boolean, value)) localStorage.setItem(key, JSON.stringify(value))
}

export const removeStorage = key => {
  localStorage.removeItem(key)
}
