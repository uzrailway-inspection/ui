import addIndex from 'ramda/src/addIndex'
import toPairs from 'ramda/src/toPairs'
import fromPairs from 'ramda/src/fromPairs'
import includes from 'ramda/src/includes'
import isNil from 'ramda/src/isNil'
import isEmpty from 'ramda/src/isEmpty'
import split from 'ramda/src/split'
import nth from 'ramda/src/nth'
import anyPass from 'ramda/src/anyPass'
import any from 'ramda/src/any'
import pathOr from 'ramda/src/pathOr'
import join from 'ramda/src/join'
import is from 'ramda/src/is'
import remove from 'ramda/src/remove'
import curry from 'ramda/src/curry'
import insert from 'ramda/src/insert'
import compose from 'ramda/src/compose'
import prop from 'ramda/src/prop'
import pipe from 'ramda/src/pipe'
import values from 'ramda/src/values'
import not from 'ramda/src/not'
import filter from 'ramda/src/filter'
import map from 'ramda/src/map'

export const mapIndexed = addIndex(map)

export const arrayObjToObj = pipe(
  map(values),
  fromPairs
)

export const omitEmpty = (obj) => fromPairs(
  filter(pair => {
    if ((!isEmpty(pair[1])) || is(Boolean, pair[1])) return pair
  }, toPairs(obj))
)

export const hasEmpty = (obj) => any(
  compose(
    anyPass([isEmpty, isNil]),
    nth(1)
  ), toPairs(obj))

export const isNotNil = compose(not, isNil)

export const isNotEmpty = compose(not, isEmpty)

export const getDataWithIds = (ids, data) =>
  filter(item => includes(prop('id', item), ids), data)

export const getIdsFromData = map(prop('id'))

export const getIdsFromUrl = compose(
  map(Number),
  filter(
    compose(
      not,
      isNaN,
      parseInt
    )
  ),
  split(','),
  pathOr('', ['ids'])
)

export const prepareDataIdsForUrl = compose(
  join(','),
  map(prop('id'))
)

export const getCurrentPage = compose(
  Number,
  pathOr(1, ['$route', 'query', 'page'])
)

export const getPageSize = compose(
  Number,
  pathOr(10, ['$route', 'query', 'page_size'])
)

export const moveArrayElement = curry((at, to, list) => insert(to, nth(at, list), remove(at, 1, list)))
