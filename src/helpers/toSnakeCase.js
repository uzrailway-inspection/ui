import map from 'ramda/src/map'
import compose from 'ramda/src/compose'
import is from 'ramda/src/is'
import { mapKeys } from '@/helpers/toCamelCase'

export const toSnake = str => {
  return str
    .replace(/\./g, '__')
    .replace(/([A-Z])/g, $1 => '_' + $1.toLowerCase())
}

const toSnakeCase = (data) => {
  if (is(Array, data)) {
    return map(toSnakeCase, data)
  }

  if (is(Object, data)) {
    return compose(
      map(toSnakeCase),
      mapKeys(toSnake)
    )(data)
  }

  return data
}

export default toSnakeCase
