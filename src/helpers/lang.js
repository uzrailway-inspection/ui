import { LANG_PREFIXES, DEFAULT_LANG_PREFIX } from '@/constants/language'

export const getDefaultLanguage = () => {
  const lang = navigator.language || navigator.userLanguage
  const browserLang = lang.match(/[^-]*/)[0]
  if (LANG_PREFIXES.includes(browserLang)) return browserLang
  return DEFAULT_LANG_PREFIX
}
