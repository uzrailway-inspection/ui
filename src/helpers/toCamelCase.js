import is from 'ramda/src/is'
import curry from 'ramda/src/curry'
import keys from 'ramda/src/keys'
import values from 'ramda/src/values'
import zipObj from 'ramda/src/zipObj'
import compose from 'ramda/src/compose'
import map from 'ramda/src/map'

export const mapKeys = curry((fn, obj) => zipObj(map(fn, keys(obj)), values(obj)))

const camelize = (str) => {
  return str
    .replace(/_/g, ' ')
    .replace(/-/g, ' ')
    .replace(/(?:^\w|[A-Z]|_|\b\w)/g, (letter, index) => {
      return index === 0 ? letter.toLowerCase() : letter.toUpperCase()
    }).replace(/\s+/g, '')
}

const toCamelCase = (data) => {
  if (is(Array, data)) {
    return map(toCamelCase, data)
  }

  if (is(Object, data)) {
    return compose(
      map(toCamelCase),
      mapKeys(camelize)
    )(data)
  }

  return data
}

export default toCamelCase
