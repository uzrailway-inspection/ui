/* ============
 * Mixins for working with text
 * ============
 *
 */
import is from 'ramda/src/is'
import { translateObject } from '@/helpers/translate'

const isFloat = n => {
  return Number(n) === n && n % 1 !== 0
}

export default {
  filters: {
    shortText: function (text, start, end) {
      if (text !== null) {
        if (start + end >= text.length) return text
        return (
          text.slice(0, start) +
          '...' +
          text.slice(text.length - end, text.length)
        )
      }
    },
    shortEmail: function (email) {
      const re = /^([a-zA-Z0-9]{1,2})(.+?)@/
      return email.replace(re, '$1...@')
    },
    slice: function (text, length) {
      if (!is(String, text)) return
      if (text.length >= length) {
        return text.slice(0, length) + '...'
      }
      return text
    },
    money: function (price, fixed = 2) {
      if (price) {
        let val
        if (isFloat(price)) {
          val = parseFloat(price).toFixed(fixed)
        } else {
          return price
        }

        return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ')
      }
      return '0'
    },
    digit: function (val) {
      if (val && isFloat(val)) {
        return val.toString().replace('.', ',')
      }
      return val
    },
    capitalize: function (value) {
      if (!value) return ''
      value = value.toString()
      return value.charAt(0).toUpperCase() + value.slice(1)
    },
    translate: function (obj, key) {
      return translateObject(key, obj)
    }
  }
}
