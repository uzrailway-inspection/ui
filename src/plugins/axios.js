import axios from 'axios'
import path from 'ramda/src/path'
import equals from 'ramda/src/equals'
import is from 'ramda/src/is'
import compose from 'ramda/src/compose'
import prop from 'ramda/src/prop'
import { API_URL } from '@/constants/api'
import VueRouter from '@/plugins/vue-router'
import i18n from '@/plugins/vue-i18n'
import store from '@/store'
import toCamelCase from '@/helpers/toCamelCase'
import { NOTIFY_ACTION, LOGOUT_ACTION } from '@/constants/actionTypes'
import * as ROUTES from '@/constants/routes'

const INTERNAL_ERROR = 500
const NOT_FOUND = 404
const UNAUTHORIZED = 401
const FORBIDDEN = 403
const CONTENT_TYPE_JSON = 'application/json'

const responseToCamelCase = (data, response) => {
  const responseContentType = path(['content-type'], response)

  if (equals(CONTENT_TYPE_JSON, responseContentType)) {
    return toCamelCase(JSON.parse(data))
  }

  if (is(Object, data) || is(Array, data)) {
    return toCamelCase(data)
  }

  return data
}

const errorInterceptors = async error => {
  const status = path(['response', 'status'], error)

  if (equals(INTERNAL_ERROR, status)) {
    return store.dispatch(NOTIFY_ACTION, {
      content: i18n.t('Internal server error'),
      value: true,
      color: 'error'
    })
  }

  if (equals(NOT_FOUND, status)) {
    return store.dispatch(NOTIFY_ACTION, {
      content: i18n.t('Not found'),
      value: true,
      color: 'error'
    })
  }

  if (equals(UNAUTHORIZED, status)) {
    await store.dispatch(LOGOUT_ACTION)
    return VueRouter.push({
      name: ROUTES.N_LOGIN
    })
  }

  if (equals(FORBIDDEN, status)) {
    return store.dispatch(NOTIFY_ACTION, {
      content: i18n.t('Access denied'),
      value: true,
      color: 'error'
    })
  }

  if (!status) {
    return store.dispatch(NOTIFY_ACTION, {
      content: i18n.t('Error connecting to the internet'),
      value: true,
      color: 'error'
    })
  }

  return Promise.reject(error)
}

axios.defaults.transformResponse = [responseToCamelCase]

axios.defaults.baseURL = API_URL

axios.interceptors.request.use(request => {
  const token = store.getters.getToken
  if (token) {
    request.headers.common.Authorization = `Token ${token}`
  }
  return request
})

axios.interceptors.response.use(
  response => response,
  errorInterceptors
)

export const getPayloadFromSuccess = prop('data')
export const getPayloadFromError = compose(
  data => Promise.reject(data),
  path(['response', 'data'])
)

export default axios
