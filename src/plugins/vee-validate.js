/* ============
 * vee-validate
 * ============
 *
 * vee-validate is a template-based validation framework for Vue.js that allows you to validate inputs and display
 * errors
 *
 * https://github.com/logaretm/vee-validate
 */

import Vue from 'vue'
import { ValidationProvider, ValidationObserver, extend, configure } from 'vee-validate'
import {
  required,
  email,
  max,
  min,
  confirmed,
  required_if as requiredIf,
  max_value as maxValue,
  min_value as minValue
} from 'vee-validate/dist/rules'
import i18n from '@/plugins/vue-i18n'

Vue.component('ValidationProvider', ValidationProvider)
Vue.component('ValidationObserver', {
  extends: ValidationObserver,
  watch: {
    '$i18n.locale': {
      deep: true,
      immediate: true,
      handler () {
        requestAnimationFrame(() => {
          this.validate()
        })
      }
    }
  }
})

configure({
  defaultMessage: (field, values) => {
    values._field_ = i18n.t(`fields.${field}`)
    return i18n.t(`validations.${values._rule_}`, values)
  }
})

extend('required', required)
extend('max', max)
extend('max_value', maxValue)
extend('min', min)
extend('min_value', minValue)
extend('email', email)
extend('required_if', requiredIf)
extend('confirmed', confirmed)
