/* ============
 * Vue I18n
 * ============
 *
 * Vue I18n is internationalization plugin for Vue.js
 *
 * http://kazupon.github.io/vue-i18n/
 */

import Vue from 'vue'
import VueI18n from 'vue-i18n'
import messages from '@/constants/i18n'
import { getDefaultLanguage } from '@/helpers/lang'

const defaultLang = getDefaultLanguage()

Vue.use(VueI18n)

const i18n = new VueI18n({
  locale: defaultLang,
  fallbackLocale: defaultLang,
  silentTranslationWarn: true,
  silentFallbackWarn: true,
  messages
})

export default i18n
