/* ============
 * vue-breadcrumbs
 * ============
 *
 * Vue breadcrumbs builds on the official vue-router package to provide simple breadcrumbs
 *
 * https://github.com/samturrell/vue-breadcrumbs
 */

import Vue from 'vue'
import VueBreadcrumbs from 'vue-breadcrumbs'

Vue.use(VueBreadcrumbs)
