/* ============
 * Vuetify
 * ============
 *
 * Vuetify is the #1 component library for Vue.js and has been in active development since 2016.
 * The goal of the project is to provide users with everything that is needed to build rich and engaging
 * web applications using the Material Design specification.
 *
 * https://vuetifyjs.com/
 */

import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import colors from 'vuetify/es5/util/colors'
import i18n from '@/plugins/vue-i18n'
import { LANG_PREFIXES } from '@/constants/language'

Vue.use(Vuetify)

export default new Vuetify({
  lang: {
    t: (key, ...params) => i18n.t(key, params),
    locales: LANG_PREFIXES,
    current: i18n.locale
  },
  theme: {
    dark: false,
    themes: {
      light: {
        primary: colors.blue.darken1
      }
    }
  }
})
