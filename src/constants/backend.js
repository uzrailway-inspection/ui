export const COMPANY_CATEGORY_GREEN = 0
export const COMPANY_CATEGORY_YELLOW = 1
export const COMPANY_CATEGORY_RED = 2

export const COMPANY_CATEGORY = [
  {
    text: 'green',
    value: COMPANY_CATEGORY_GREEN
  },
  {
    text: 'yellow',
    value: COMPANY_CATEGORY_YELLOW
  },
  {
    text: 'red',
    value: COMPANY_CATEGORY_RED
  }
]
