export const LANGUAGES = [
  {
    text: 'Ўз',
    value: 'uz'
  },
  {
    text: 'Ру',
    value: 'ru'
  }
]

export const LANG_PREFIXES = ['uz', 'ru']

export const DEFAULT_LANG_PREFIX = 'ru'
