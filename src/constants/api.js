export const API_HOST = process.env.VUE_APP_API_BASE_URL || 'http://127.0.0.1:8000'
export const API_VERSION = 'v1'
export const API_URL = `${API_HOST}/api/${API_VERSION}`

// Auth API
const AUTH = 'auth'
export const LOGIN = `/${AUTH}/authenticate/`
export const ME = `/${AUTH}/me/`
export const GET_PERMISSIONS = `/${AUTH}/permissions/`

// AVATAR API
export const AVATAR_UPLOAD = `/${AUTH}/avatar/`

// USERS API
export const GET_USER_LIST = `/${AUTH}/users/`
export const USER_CREATE = `/${AUTH}/users/`
export const USER_DETAIL = `/${AUTH}/users/%d/`
export const USER_BULK_DELETE = `/${AUTH}/users/destroy-bulk/`

// AUTH GROUP
export const GET_GROUP_LIST = `/${AUTH}/groups/`
export const GROUP_CREATE = `/${AUTH}/groups/`
export const GROUP_DETAIL = `/${AUTH}/groups/%d/`

// COMPANY
const COMPANY = 'company'

// COMPANY API
export const GET_COMPANY_LIST = `/${COMPANY}/company/`
export const COMPANY_CREATE = `/${COMPANY}/company/`
export const COMPANY_DETAIL = `/${COMPANY}/company/%d/`
export const COMPANY_BULK_DELETE = `/${COMPANY}/company/destroy-bulk/`

// ORGANIZATION API
const ORGANIZATION = 'main-organization'

export const GET_ORGANIZATION_LIST = `/${COMPANY}/${ORGANIZATION}/`
export const ORGANIZATION_CREATE = `/${COMPANY}/${ORGANIZATION}/`
export const ORGANIZATION_DETAIL = `/${COMPANY}/${ORGANIZATION}/%d/`
export const ORGANIZATION_BULK_DELETE = `/${COMPANY}/${ORGANIZATION}/destroy-bulk/`

// CATEGORY API
const CATEGORY = 'category'

export const GET_CATEGORY_LIST = `/${COMPANY}/${CATEGORY}/`
export const CATEGORY_CREATE = `/${COMPANY}/${CATEGORY}/`
export const CATEGORY_DETAIL = `/${COMPANY}/${CATEGORY}/%d/`
export const CATEGORY_BULK_DELETE = `/${COMPANY}/${CATEGORY}/destroy-bulk/`

// STATION API
const STATION = 'station'

export const GET_STATION_LIST = `/${COMPANY}/${STATION}/`
export const STATION_CREATE = `/${COMPANY}/${STATION}/`
export const STATION_DETAIL = `/${COMPANY}/${STATION}/%d/`
export const STATION_BULK_DELETE = `/${COMPANY}/${STATION}/destroy-bulk/`

// FACTOR API
const FACTOR = 'factor'

export const GET_FACTOR_LIST = `/${COMPANY}/${FACTOR}/`
export const FACTOR_CREATE = `/${COMPANY}/${FACTOR}/`
export const FACTOR_DETAIL = `/${COMPANY}/${FACTOR}/%d/`
export const CHANGE_FACTOR_POSITION = `/${COMPANY}/${FACTOR}/%d/change-position/`
export const FACTOR_BULK_DELETE = `/${COMPANY}/${FACTOR}/destroy-bulk/`

// RATING API
const RATING = 'rating'

export const GET_RATING_LIST = `/${COMPANY}/${RATING}/`
export const RATING_DETAIL = `/${COMPANY}/${RATING}/%d/`
export const GET_RATING_STATISTIC = `/${COMPANY}/${RATING}/%d/get-statistics/`
export const GET_RATING_FULL_STATISTIC = `/${COMPANY}/${RATING}/%d/get-full-statistics/`
export const GET_RATING_FOR_INSPECTOR_LIST = `/${COMPANY}/${RATING}/get-rating-for-inspector/`

// REPORT API
const REPORT = 'report'

export const GET_REPORT_LIST = `/${COMPANY}/${REPORT}/`
export const REPORT_CREATE = `/${COMPANY}/${REPORT}/`
export const DOWNLOAD_REPORT = `/${COMPANY}/${REPORT}/%d/`

// REVISION API
const REVISION = 'revision'

export const GET_REVISION_LIST = `/${COMPANY}/${REVISION}/`
export const REVISION_CREATE = `/${COMPANY}/${REVISION}/`
export const REVISION_CONFIRM = `/${COMPANY}/${REVISION}/%d/confirm/`

// COMMON
const COMMON = 'common'

// REGION API
const REGION = 'region'

export const GET_REGION_LIST = `/${COMMON}/${REGION}/`
export const REGION_CREATE = `/${COMMON}/${REGION}/`
export const REGION_DETAIL = `/${COMMON}/${REGION}/%d/`
export const REGION_BULK_DELETE = `/${COMMON}/${REGION}/destroy-bulk/`

// DISTRICT API
const DISTRICT = 'district'

export const GET_DISTRICT_LIST = `/${COMMON}/${DISTRICT}/`
export const DISTRICT_CREATE = `/${COMMON}/${DISTRICT}/`
export const DISTRICT_DETAIL = `/${COMMON}/${DISTRICT}/%d/`
export const DISTRICT_BULK_DELETE = `/${COMMON}/${DISTRICT}/destroy-bulk/`
