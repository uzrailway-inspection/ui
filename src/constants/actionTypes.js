// LOGIN
export const LOGIN_ACTION = 'loginAction'
export const LOGOUT_ACTION = 'logoutAction'
export const TOGGLE_THEME_ACTION = 'toggleThemeAction'

// GENERAL
export const GET_ME_ACTION = 'getMeAction'
export const NOTIFY_ACTION = 'notifyAction'
export const GET_PERMISSIONS_ACTION = 'getPermissionsAction'

// USER
export const USER = 'user'

export const GET_USER_LIST_ACTION = 'getUserListAction'
export const USER_UPDATE_ACTION = 'userUpdateAction'
export const USER_CREATE_ACTION = 'userCreateAction'
export const USER_DETAIL_ACTION = 'userDetailAction'
export const USER_DELETE_ACTION = 'userDeleteAction'
export const USER_BULK_DELETE_ACTION = 'userBulkDeleteAction'

// AUTH GROUP
export const USER_GROUP = 'user_group'

export const GET_GROUP_LIST_ACTION = 'getGroupListAction'
export const GROUP_UPDATE_ACTION = 'groupUpdateAction'
export const GROUP_CREATE_ACTION = 'groupCreateAction'
export const GROUP_DETAIL_ACTION = 'groupDetailAction'
export const GROUP_DELETE_ACTION = 'groupDeleteAction'

// COMPANY
export const COMPANY = 'company'

export const GET_COMPANY_LIST_ACTION = 'getCompanyListAction'
export const COMPANY_UPDATE_ACTION = 'companyUpdateAction'
export const COMPANY_CREATE_ACTION = 'companyCreateAction'
export const COMPANY_DETAIL_ACTION = 'companyDetailAction'
export const COMPANY_DELETE_ACTION = 'companyDeleteAction'
export const COMPANY_BULK_DELETE_ACTION = 'companyBulkDeleteAction'

// ORGANIZATION
export const ORGANIZATION = 'organization'

export const GET_ORGANIZATION_LIST_ACTION = 'getOrganizationListAction'
export const ORGANIZATION_UPDATE_ACTION = 'organizationUpdateAction'
export const ORGANIZATION_CREATE_ACTION = 'organizationCreateAction'
export const ORGANIZATION_DETAIL_ACTION = 'organizationDetailAction'
export const ORGANIZATION_DELETE_ACTION = 'organizationDeleteAction'
export const ORGANIZATION_BULK_DELETE_ACTION = 'organizationBulkDeleteAction'

// REGION
export const REGION = 'region'

export const GET_REGION_LIST_ACTION = 'getRegionListAction'
export const REGION_UPDATE_ACTION = 'regionUpdateAction'
export const REGION_CREATE_ACTION = 'regionCreateAction'
export const REGION_DETAIL_ACTION = 'regionDetailAction'
export const REGION_DELETE_ACTION = 'regionDeleteAction'
export const REGION_BULK_DELETE_ACTION = 'regionBulkDeleteAction'

// DISTRICT
export const DISTRICT = 'district'

export const GET_DISTRICT_LIST_ACTION = 'getDistrictListAction'
export const DISTRICT_UPDATE_ACTION = 'districtUpdateAction'
export const DISTRICT_CREATE_ACTION = 'districtCreateAction'
export const DISTRICT_DETAIL_ACTION = 'districtDetailAction'
export const DISTRICT_DELETE_ACTION = 'districtDeleteAction'
export const DISTRICT_BULK_DELETE_ACTION = 'cistrictBulkDeleteAction'

// CATEGORY
export const CATEGORY = 'category'

export const GET_CATEGORY_LIST_ACTION = 'getCategoryListAction'
export const CATEGORY_UPDATE_ACTION = 'categoryUpdateAction'
export const CATEGORY_CREATE_ACTION = 'categoryCreateAction'
export const CATEGORY_DETAIL_ACTION = 'categoryDetailAction'
export const CATEGORY_DELETE_ACTION = 'categoryDeleteAction'
export const CATEGORY_BULK_DELETE_ACTION = 'categoryBulkDeleteAction'

// STATION
export const STATION = 'station'

export const GET_STATION_LIST_ACTION = 'getStationListAction'
export const STATION_UPDATE_ACTION = 'stationUpdateAction'
export const STATION_CREATE_ACTION = 'stationCreateAction'
export const STATION_DETAIL_ACTION = 'stationDetailAction'
export const STATION_DELETE_ACTION = 'stationDeleteAction'
export const STATION_BULK_DELETE_ACTION = 'stationBulkDeleteAction'

// FACTOR
export const FACTOR = 'factor'

export const GET_FACTOR_LIST_ACTION = 'getFactorListAction'
export const FACTOR_UPDATE_ACTION = 'factorUpdateAction'
export const FACTOR_CREATE_ACTION = 'factorCreateAction'
export const FACTOR_DETAIL_ACTION = 'factorDetailAction'
export const FACTOR_DELETE_ACTION = 'factorDeleteAction'
export const CHANGE_FACTOR_POSITION_ACTION = 'changeFactorPositionAction'
export const FACTOR_BULK_DELETE_ACTION = 'factorBulkDeleteAction'

// REVISION
export const REVISION = 'revision'

export const GET_REVISION_LIST_ACTION = 'getRevisionListAction'
export const REVISION_CREATE_ACTION = 'revisionCreateAction'
export const REVISION_CONFIRM_ACTION = 'revisionConfirmAction'

// RATING
export const RATING = 'rating'

export const GET_RATING_LIST_ACTION = 'getRatingListAction'
export const GET_RATING_FOR_INSPECTOR_LIST_ACTION = 'getRatingForInspectionListAction'
export const RATING_DETAIL_ACTION = 'ratingDetailAction'
export const GET_RATING_STATISTIC_ACTION = 'getRatingStatisticAction'
export const GET_RATING_FULL_STATISTIC_ACTION = 'getRatingFullStatisticAction'

// REPORT
export const REPORT = 'report'

export const GET_REPORT_LIST_ACTION = 'getReportListAction'
export const EXPORT_REPORT_ACTION = 'exportReportAction'
export const DOWNLOAD_REPORT_ACTION = 'downloadReportAction'
