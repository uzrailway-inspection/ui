import { LANG_PREFIXES } from '@/constants/language'

const LANG_PREFIX = LANG_PREFIXES.join('|')
export const MAIN_ROOT_PATH = `/:lang(${LANG_PREFIX})/`

const ID = ':id(\\d+)'

// LOGIN-IN
export const N_LOGIN = 'login'
export const LOGIN_PATH = 'login/'

// CRUD
export const LIST = ''
export const UPDATE = `update/${ID}/`
export const CREATE = 'create/'
export const DETAIL = `${ID}/`

// USERS
export const USERS_PATH = 'users/'

export const USER_PATH = 'user'

export const USER_LIST = 'user-list'
export const USER_UPDATE = 'user-update'
export const USER_CREATE = 'user-create'

export const GROUPS_PATH = 'groups/'

export const GROUP_LIST = 'group-list'
export const GROUP_UPDATE = 'group-update'
export const GROUP_CREATE = 'group-create'

// RATING
export const RATING_PATH = 'rating/'

export const RATING_LIST = 'rating-list'

// INSTRUCTION
export const INSTRUCTION_PATH = 'instruction/'

export const INSTRUCTION = 'instruction'

// REPORT
export const REPORT_PATH = 'report/'
export const REPORT_LIST = 'report-list'

// COMMON
export const COMMON_PATH = 'common/'

export const COMPANY_PATH = 'company/'
export const COMPANY_LIST = 'company-list'
export const COMPANY_UPDATE = 'company-update'
export const COMPANY_CREATE = 'company-create'

export const ORGANIZATION_PATH = 'organization/'
export const ORGANIZATION_LIST = 'organization-list'
export const ORGANIZATION_UPDATE = 'organization-update'
export const ORGANIZATION_CREATE = 'organization-create'

export const REGION_PATH = 'region/'
export const REGION_LIST = 'region-list'
export const REGION_UPDATE = 'region-update'
export const REGION_CREATE = 'region-create'

export const DISTRICT_PATH = 'district/'
export const DISTRICT_LIST = 'district-list'
export const DISTRICT_UPDATE = 'district-update'
export const DISTRICT_CREATE = 'district-create'

export const CATEGORY_PATH = 'category/'
export const CATEGORY_LIST = 'category-list'
export const CATEGORY_UPDATE = 'category-update'
export const CATEGORY_CREATE = 'category-create'

export const STATION_PATH = 'station/'
export const STATION_LIST = 'station-list'
export const STATION_UPDATE = 'station-update'
export const STATION_CREATE = 'station-create'

export const FACTOR_PATH = 'factor/'
export const FACTOR_LIST = 'factor-list'
export const FACTOR_UPDATE = 'factor-update'
export const FACTOR_CREATE = 'factor-create'

export const REVISION_PATH = 'revision/'
export const REVISION_LIST = 'revision-list'
export const REVISION_CREATE = 'revision-create'
