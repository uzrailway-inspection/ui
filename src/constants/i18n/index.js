import UZ from './uz'
import RU from './ru'

export default {
  uz: UZ,
  ru: RU
}
