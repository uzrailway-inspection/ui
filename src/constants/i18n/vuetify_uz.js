export default {
  badge: 'белги',
  close: 'Ёпмоқ',
  dataIterator: {
    pageText: '{0}-{1} of {2}',
    noResultsText: 'Мос ёзувлар топилмади',
    loadingText: 'Рўйхат юкланмоқда...'
  },
  dataTable: {
    itemsPerPageText: 'Сахифадаги элементлар сони:',
    ariaLabel: {
      sortDescending: ': Камайиш тартибида саралаш. Саралашни ўчириш учун активлаштиринг.',
      sortAscending: ': Ўсиш тартибида саралаш. Саралашни ўчириш учун активлаштиринг.Камайиш тартибида саралаш учун активлаштиринг.',
      sortNone: ': Сараланмаган. Ўсиш тартибида саралаш учун активлаштиринг.'
    },
    sortBy: 'Бўйича саралаш'
  },
  dataFooter: {
    pageText: '{2} дан {0}-{1}',
    itemsPerPageText: 'Элементлар:',
    itemsPerPageAll: 'Барчаси',
    nextPage: 'Кеёинги сахифа',
    prevPage: 'Олдинги сахифа',
    firstPage: 'Биринчи сахифа',
    lastPage: 'Охирги сахифа'
  },
  datePicker: {
    itemsSelected: '{0} танланди'
  },
  noDataText: 'Маьлумотлар мавжуд эмас',
  carousel: {
    prev: 'Олдинги',
    next: 'Кейинги'
  },
  calendar: {
    moreEvents: 'Яна {0} та'
  },
  fileInput: {
    counter: 'Афйллар сони: {0}',
    counterSize: 'Файллар: {0} (борйўғи {1})'
  }
}
