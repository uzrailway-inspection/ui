import * as MENU from '@/constants/routes'
import { USER_LIST } from '@/constants/routes'

export const menu = [
  {
    name: MENU.RATING_LIST,
    title: 'companies',
    icon: 'mdi-chart-line',
    permissions: ['list_rating', 'get_rating_for_inspector_rating']
  },
  {
    name: MENU.INSTRUCTION,
    title: 'instruction',
    icon: 'mdi-help'
  },
  {
    name: MENU.COMPANY_LIST,
    title: 'common',
    icon: 'mdi-domain',
    permissions: 'list_company'
  },
  {
    name: MENU.REPORT_LIST,
    title: 'archive',
    icon: 'mdi-package-down',
    permissions: 'list_report'
  },
  {
    name: USER_LIST,
    title: 'users',
    icon: 'mdi-account-multiple',
    permissions: 'list_user'
  }
]

export const COMMON_MENU = [
  {
    name: MENU.COMPANY_LIST,
    title: 'companies',
    permissions: 'list_company'
  },
  {
    name: MENU.REVISION_LIST,
    title: 'revisions',
    permissions: 'list_revision'
  },
  {
    name: MENU.ORGANIZATION_LIST,
    title: 'organizations',
    permissions: 'list_mainorganization'
  },
  {
    name: MENU.REGION_LIST,
    title: 'regions',
    permissions: 'list_region'
  },
  {
    name: MENU.DISTRICT_LIST,
    title: 'districts',
    permissions: 'list_district'
  },
  {
    name: MENU.CATEGORY_LIST,
    title: 'categories',
    permissions: 'list_category'
  },
  {
    name: MENU.STATION_LIST,
    title: 'stations',
    permissions: 'list_station'
  },
  {
    name: MENU.FACTOR_LIST,
    title: 'factors',
    permissions: 'list_factor'
  }
]

export const USER_MENU = [
  {
    name: MENU.USER_LIST,
    title: 'users',
    permissions: 'list_user'
  },
  {
    name: MENU.GROUP_LIST,
    title: 'groups',
    permissions: 'list_group'
  }
]
