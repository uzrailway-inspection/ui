FROM node:lts-alpine

# install express.js globally
RUN npm install -g express connect-history-api-fallback

# set work dir as main
WORKDIR /app

# copy all fiels to 'app'
COPY . .

CMD [ "node", "server.js" ]
