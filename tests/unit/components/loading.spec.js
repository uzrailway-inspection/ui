import { expect } from 'chai'
import { mount, createLocalVue } from '@vue/test-utils'
import Loading from '@/components/Loading.vue'
import vuetify from '@/plugins/vuetify'

const localVue = createLocalVue()

const factory = () => {
  return mount(Loading, { propsData: { loading: true }, vuetify, localVue })
}

describe('Loading.vue', () => {
  it('loading on', () => {
    const wrapper = factory()
    expect(wrapper.find('.v-overlay--active').exists()).to.equal(true)
  })

  it('loading off', (done) => {
    const wrapper = factory()
    setTimeout(() => {
      wrapper.setProps({ loading: false })
      wrapper.vm.$nextTick(() => {
        expect(wrapper.find('.v-overlay--active').exists()).to.equal(false)
        done()
      })
    }, 1000)
  })
})
