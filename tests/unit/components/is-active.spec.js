import { expect } from 'chai'
import { mount, createLocalVue } from '@vue/test-utils'
import IsActive from '@/components/IsActive.vue'
import vuetify from '@/plugins/vuetify'

const localVue = createLocalVue()

const factory = () => {
  return mount(IsActive, { propsData: { active: true }, localVue, vuetify })
}

describe('IsActive.vue', () => {
  it('status on', () => {
    const wrapper = factory()
    expect(wrapper.classes()).to.be.an('array').and.include('success--text')
  })

  it('status off', () => {
    const wrapper = factory()
    wrapper.setProps({ active: false })
    wrapper.vm.$nextTick(() => {
      expect(wrapper.classes()).to.be.an('array').and.include('grey--text')
    })
  })
})
