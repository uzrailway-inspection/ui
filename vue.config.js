const path = require('path')
const webpack = require('webpack')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
  .BundleAnalyzerPlugin

module.exports = {
  productionSourceMap: false,
  configureWebpack: {
    resolve: {
      extensions: ['.js', '.vue', '.json'],
      alias: {
        '~': path.resolve(__dirname, 'src/'),
        '@': path.resolve(__dirname, 'src/')
      }
    },
    plugins: [
      new BundleAnalyzerPlugin({ analyzerMode: 'static', openAnalyzer: false }),
      new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /uz|ru/)
    ]
  },
  transpileDependencies: [
    'vuetify'
  ],
  css: {
    loaderOptions: {
      sass: {
        prependData: "@import '@/styles/variables.scss'"
      },
      scss: {
        prependData: "@import '@/styles/variables.scss';"
      }
    }
  }
}
